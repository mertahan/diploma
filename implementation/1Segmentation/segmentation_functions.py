"""
   this file contains the methods used for segmentation and methods for saving the result images
   each segmentation method takes:
    input:
        name of the input file,
        name of the output file,
        optionally : preloaded input file
    no output:
        saves the result of segmentation combined with the original picture
"""

import skimage
import numpy as np
import matplotlib.pyplot as plt
import os
from skimage import io
from skimage import filters
import cv2
from skimage.feature import canny
from scipy import ndimage as ndi


"""
    merges two images: 
    imfloat: binary picture of segmentation
    im_con_float: float picture of the original image
    alpha: expresses what part of convex combination is im_float
    result is the convex combination of pictures
"""
# def merge_float_ims1(im_float, im_con_float, alpha=0.25):
#     res = 255.0 * np.stack([im_float, im_float*0, im_float*0], axis=2)
#     res_con = 255.0 * np.stack([im_con_float*0.33, im_con_float*0.33, im_con_float*0.33], axis=2)
#     res/=np.amax(res)
#     res_con /= np.amax(res_con)
#     ult = alpha * res + (1-alpha) * res_con
#     return ult/np.max(ult)

"""
    merges two images: 
    imfloat: binary picture of segmentation
    im_con_float: float picture of the original image
    result is the im_con_float picture behind the reddened im_float picture (mask)
"""
def merge_float_ims(im_float, im_con_float, alpha=0.25):
    im_con_float = im_con_float * (1-im_float)
    res_con = 255.0 * np.stack([im_con_float*0.33, im_con_float*0.33, im_con_float*0.33], axis=2)
    #res_con = res_con * (1-im_float)

    res = 255.0 * np.stack([im_float, im_float * 0, im_float * 0], axis=2)
    res/=np.amax(res)
    #res_con /= np.amax(res_con)
    ult = res + res_con
    return ult/np.max(ult)

# def merge_cv_ims(img, img1, alpha=0.25):
#     img = img / np.amax(img)
#     img[:, :, 1] = img[:, :, 1] / 3
#     img[:, :, 2] = img[:, :, 2] / 3
#     img1 = img1 / np.amax(img1)
#     img3 = alpha * img + (1-alpha) * img1
#     return img3
def save_combined_image(filename, im):
    plt.imsave(filename, im)

"""saves the original image"""
def save_original(image_name, result_name, orig_im_float=""):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv_float

    save_combined_image(result_name, orig_im_float)


"""thresholding by value obtained from mean+erosion from picture where it worked well"""
def manual_thresh(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv_float

    thresh = 0.001998931868467231  # obtained as np.min(orig_im_float[min_img>0.5]) from mean_thresh_er
                                    # from 1500cells 24h_C02_T0001F001L01A01Z01C01.tif
    mask = (orig_im_float > thresh)*1.0

    if save:
        res = merge_float_ims(mask, orig_im_float, 0.2)
        save_combined_image(result_name, res)
    return mask


"""thresholding by otsu - scimage.filters.threshold_otsu()"""
def otsu_scimage(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than img

    val = filters.threshold_otsu(orig_im_float)
    mask = (orig_im_float > val)*1.0

    if save:
        res = merge_float_ims(mask, orig_im_float, 0.2)
        save_combined_image(result_name, res)
    return mask


"""thresholding by otsu - cv2.threshold()"""
def otsu_cv(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than img

    img = cv2.imread(image_name, 0)
    blur = cv2.GaussianBlur(img, (5, 5), 0)
    ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    th3 = np.array(th3, dtype=np.float)/255.0

    if save:
        res = merge_float_ims(th3, orig_im_float, 0.2)
        save_combined_image(result_name, res)
    return th3


"""thresholding by adaptive mean - cv2.adaptiveThreshold()"""
def adaptive_mean_thresh(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv img

    img = cv2.imread(image_name, 0)
    img = img.copy() * 255.0
    img /= np.amax(img)
    img = np.uint8(img * 255)
    img = cv2.medianBlur(img, 5)
    th2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)

    th2 = 1 - (th2/255)

    if save:
        res = merge_float_ims(th2, orig_im_float, 0.3)
        save_combined_image(result_name, res)

    return th2


"""thresholding by adaptive gaussian - cv2.adaptiveThreshold()"""
def adaptive_gaussian_thresh(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv img

    img = cv2.imread(image_name, 0)
    img = img.copy() * 255.0
    img /= np.amax(img)
    img = np.uint8(img * 255)
    img = cv2.medianBlur(img, 5)
    th3 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

    th3 = 1 - (th3 / 255)

    if save:
        res = merge_float_ims(th3, orig_im_float, 0.3)
        save_combined_image(result_name, res)

    return th3

    #plt.imsave(result_name, th3)


"""threshold by adaptive gaussian and contours filled by ndi"""
def filled_adaptive_gaussian(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv img

    img = cv2.imread(image_name, 0)
    img = img.copy() * 255.0
    img /= np.amax(img)
    img = np.uint8(img * 255)
    img = cv2.medianBlur(img, 5)
    th3 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    edges = canny(th3)
    fill_nuclei = ndi.binary_fill_holes(edges)

    if save:
        res = merge_float_ims(fill_nuclei, orig_im_float, 0.3)
        save_combined_image(result_name, res)
    return fill_nuclei


"""mean threshold"""
def mean_thresh(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float):
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv img

    im_float = orig_im_float / np.amax(orig_im_float)
    im_float[im_float >= np.mean(im_float)] = 1
    im_float[im_float < 1] = 0

    if save:
        res = merge_float_ims(im_float, orig_im_float, 0.2)
        save_combined_image(result_name, res)

    return im_float

"""mean threshold with erosion to suppress noise"""
def mean_n_erosion(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than img

    im_float = orig_im_float/ np.amax(orig_im_float)
    im_float[im_float >= np.mean(im_float)] = 1
    im_float[im_float < 1] = 0

    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    #kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))


    min_img = cv2.erode(im_float, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)
    #thresh = np.min(orig_im_float[min_img>0.5])
    #filt = ndi.median_filter(im_float, size=15)
    if save:
        res = merge_float_ims(dil_img, orig_im_float, 0.2)
        save_combined_image(result_name, res)
    return dil_img


"""quantile threshold"""
def quant_thresh(image_name, result_name, orig_im_float="", quant=0.8, save=True):
    if len(orig_im_float):
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than cv img

    im_float = orig_im_float / np.amax(orig_im_float)
    im_float[im_float >= np.quantile(im_float, quant)] = 1
    im_float[im_float < 1] = 0

    if save:
        res = merge_float_ims(im_float, orig_im_float, 0.2)
        rname = result_name[:-4] + str(quant) + ".png"
        save_combined_image(rname, res)

    return im_float


"""quantile threshold with erosion to suppress noise"""
def quant_n_erosion(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than img

    im_float = orig_im_float/ np.amax(orig_im_float)
    im_float[im_float >= np.quantile(im_float, 0.8)] = 1
    im_float[im_float < 1] = 0

    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    #kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))


    min_img = cv2.erode(im_float, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)

    if save:
        res = merge_float_ims(dil_img, orig_im_float, 0.2)
        save_combined_image(result_name, res)

    return dil_img


"""combines mean threshold with quantile threshold and filters out the noise by erosion"""
def comb_mean_med_n_erosion(image_name, result_name, orig_im_float="", save = True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than img

    im_float = orig_im_float / np.amax(orig_im_float)
    # im_float = ndi.gaussian_filter(im_float, sigma=5) # to test without median
    alpha = 0.7
    comb = alpha * np.mean(im_float) + (1 - alpha) * np.quantile(im_float, 0.8)
    im_float[im_float >= comb] = 1
    im_float[im_float < 1] = 0

    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    #kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10))


    min_img = cv2.erode(im_float, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)

    if save:
        res = merge_float_ims(dil_img, orig_im_float, 0.2)
        save_combined_image(result_name, res)

    return dil_img


""" combines mean threshold with quantile threshold and filters out the noise by median filter """
def comb_mean_med(image_name, result_name, orig_im_float="", save=True):
    if len(orig_im_float) == 0:
        mush = io.imread(image_name)
        orig_im_float = skimage.img_as_float(mush)  # somehow it shows more than img

    im_float = orig_im_float/ np.amax(orig_im_float)
    #im_float = ndi.gaussian_filter(im_float, sigma=5) # to test without median
    alpha = 0.7
    comb = alpha * np.mean(im_float) + (1-alpha) * np.quantile(im_float, 0.8)
    im_float[im_float >= comb] = 1
    im_float[im_float < 1] = 0

    mask = ndi.median_filter(im_float, size=15)

    if save:
        res = merge_float_ims(mask, orig_im_float, 0.2)
        save_combined_image(result_name, res)

    return mask


import os
import sys
#import cv2

from PIL import Image
# https://github.com/Belval/opencv-mser/blob/master/mser.py

"""
    This is a simple example on how to use OpenCV for MSER
"""
"""mser from github"""
def mser(cv_image):
    vis = cv_image.copy()
    mser = cv2.MSER_create()
    regions, _ = mser.detectRegions(cv_image)
    for p in regions:
        xmax, ymax = np.amax(p, axis=0)
        xmin, ymin = np.amin(p, axis=0)
        cv2.rectangle(vis, (xmin,ymax), (xmax,ymin), (0, 255, 0), 1)
    return vis


"""segmentation by maximal stable region cv2.MSER_create().detextRegions()"""
def mser_segm(image_name, result_name, orig_im_float="", save=True):

    cv2.imwrite(result_name, mser(cv2.imread(image_name, 0)))
    mask = mser(cv2.imread(image_name, 0))
    mask[mask>1] = 1
    mask[mask<0.5] = 0
    mask[mask>=0.5] = 1

    if save:
        res = merge_float_ims(mask, orig_im_float, 0.8)
        save_combined_image(result_name, res)
    return mask




