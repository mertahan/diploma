"""

   usage:
        python3 seg_files --src 1500_cells24h --dest mymasks
   this script loads the input images names from SOURCE_DIR and runs various segmentation methods
   obtained segmentation images are saved to RES_DIR with corresponding addition to the file name according
   to the used method

   parameters:
       --src SOURCE_DIR
       --dest RES_DIR
"""
import numpy as np
#np.set_printoptions(threshold=np.inf)
from matplotlib import pyplot as plt

import segmentation_functions as sf
import load_input
import os
from skimage import io
import skimage

# names to be appended to file names corresponding to used methods
otsu_scimage, otsu_cv, adaptive_gauss, adaptive_mean, filled_adap_gauss, mean, mean_er, quant, quant_er, \
comb, comb_er, manual, mser = \
    ["0_otsu_sci", "1_otsu_cv", "2_adap_gaus", "3_adap_mean", "4_filled_adap_gaus", "5_mean", "6_mean_er", "7_quant",
     "8_quant_er", "9_comb", "91_comb_er", "92_manual", "93_mser"]
#resdir = os.path.join(load_input.result_dir, "results1/")
resdir = load_input.result_dir

a = 0
pairs = load_input.file_pairs   # each pair contains the name of the fluorescence image and the name of the
                                      # contrast microscopy image
total = len(pairs)

# files that are just dark with noise
thrash_list = [
    "1500cells 24h_C02_T0001F001L01A01Z01C01.tif"
    "1500cells 24h_C04_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C05_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C07_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C09_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C14_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C15_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C16_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_C17_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_D02_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_D07_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_D09_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_D14_T0001F001L01A01Z01C01.tif",
    "1500cells 24h_D15_T0001F001L01A01Z01C01.tif",

    "1500cells before_C02_T0001F001L01A01Z01C01.tif",
    "1500cells before_C04_T0001F001L01A01Z01C01.tif",
    "1500cells before_C05_T0001F001L01A01Z01C01.tif",
    "1500cells before_C09_T0001F001L01A01Z01C01.tif",
    "1500cells before_C14_T0001F001L01A01Z01C01.tif",
    "1500cells before_C15_T0001F001L01A01Z01C01.tif",
    "1500cells before_C16_T0001F001L01A01Z01C01.tif",
    "1500cells before_C17_T0001F001L01A01Z01C01.tif",
    "1500cells before_D02_T0001F001L01A01Z01C01.tif",
    "1500cells before_D09_T0001F001L01A01Z01C01.tif",
    "1500cells before_D14_T0001F001L01A01Z01C01.tif",
    "1500cells before_D15_T0001F001L01A01Z01C01.tif",

    "1500cells 72h_C02_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C04_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C05_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C09_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C14_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C15_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C16_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_C17_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_D02_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_D09_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_D14_T0001F001L01A01Z01C01.tif",
    "1500cells 72h_D15_T0001F001L01A01Z01C01.tif"

    ]


for im_fl, im_con in pairs:  # processes each fluorescent image with chosen segmentation method
    a+=1
    print("\r{}/{}".format(a, total))

    if im_fl in thrash_list:
        continue

    nm = im_fl.split(".")
    nm = nm[0]
    file_name = os.path.join(load_input.datadir, im_fl)

    mush = io.imread(file_name)
    orig_im_float = skimage.img_as_float(mush)

    # sf.save_original(file_name, "{}/{}_{}.png".format(resdir, nm, "00_original"), orig_im_float)
    # sf.otsu_scimage(file_name, "{}/{}_{}.png".format(resdir, nm, otsu_scimage), orig_im_float)
    # sf.otsu_cv(file_name, "{}/{}_{}.png".format(resdir ,nm, otsu_cv), orig_im_float)
    # sf.adaptive_gaussian_thresh(file_name, "{}/{}_{}.png".format(resdir, nm, adaptive_gauss), orig_im_float)
    # sf.adaptive_mean_thresh(file_name, "{}/{}_{}.png".format(resdir, nm, adaptive_mean), orig_im_float)
    # sf.filled_adaptive_gaussian(file_name, "{}/{}_{}.png".format(resdir, nm, filled_adap_gauss), orig_im_float)
    # sf.mean_thresh(file_name, "{}/{}_{}.png".format(resdir, nm, mean), orig_im_float)
    # sf.mean_n_erosion(file_name, "{}/{}_{}.png".format(resdir, nm, mean_er), orig_im_float)
    # sf.quant_thresh(file_name, "{}/{}_{}.png".format(resdir, nm, quant), orig_im_float, quant=0.9)
    # sf.quant_n_erosion(file_name, "{}/{}_{}.png".format(resdir, nm, quant_er), orig_im_float)
    # sf.comb_mean_med(file_name, "{}/{}_{}.png".format(resdir, nm, comb), orig_im_float)
    # sf.comb_mean_med_n_erosion(file_name, "{}/{}_{}.png".format(resdir, nm, comb_er), orig_im_float)
    mask = sf.manual_thresh(file_name, "{}/{}_{}.png".format(resdir, nm, manual), orig_im_float, save=False)
    # sf.mser_segm(file_name, "{}/{}_{}.png".format(resdir, nm, mser), orig_im_float)
    mask *= 255
    fname = "{}/{}.png".format(resdir, nm)
    io.imsave(fname, mask.astype(np.uint8))







