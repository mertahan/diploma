import random
import pickle
import os
import argparse
from skimage import measure as skm
import numpy as np
from numpy.linalg import norm as euklid
from skimage import io
import cv2
import scipy.ndimage as nd
from sklearn.feature_selection import SelectKBest, chi2

import descriptors
from pprint import pprint

import extract_features
from extract_features import dump
from extract_features import Imdata

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from sklearn.model_selection import train_test_split

from matplotlib import pyplot as plt

"""
creates the dictionary with that maps each treatment to the codes
returns: dictionary, enum for treatments
"""
def create_dict():
    Topotecan = ["C02", "C03", "C04", "C05", "D02", "D03", "D04", "D05"]
    Daunorubicin = ["C14", "C15", "C16", "C17", "D14", "D15", "D16", "D17"]
    Etoposide = ["C06", "C07", "C08", "C09", "D06", "D07", "D08", "D09"]
    DMSO = ["C18", "C19", "C20", "C21", "D18", "D19", "D20", "D21"]
    no = ["C10", "C11", "C12", "C13", "D10", "D11", "D12", "D13", "C22", "C23", "D22", "D23"]
    d = {"top": Topotecan, "dau": Daunorubicin, "eto": Etoposide, "DMSO": DMSO, "no": no}
    en = {}
    for i, a in enumerate(d):
        en[a] = i
    return d, en

"""
returns the accuracy of the prediction compared to ground truth
param: predicted - array of the predicted categories
       ground - array of the ground truth categories
returns: accuracy [0,1]
"""
def accuracy(predicted, ground):
    rr = np.array(predicted) - np.array(ground)
    right = len(rr[rr == 0])
    acc = right / len(predicted)
    return acc

"""
selects the most descriptive features from the feature vectors
according to chi2 criterium
param: X  - matrix of size (num_samples, feature_vector_size)
       y  - corresponding class labels to each row of X
       num - the desired length of the feature vector
returns: X_new - matrix of size (num_samples, num),
                each row is the reduced feature vector
         a  - indices of selected features
"""
def feature_select(X,y,num):
    X = np.array(X)
    y = np.array(y)
    selector = SelectKBest(chi2, k=num)
    X_new = selector.fit_transform(X, y)
    a=selector.get_support(indices=True)
    return X_new, a

"""
takes the input pickle of Imdata type,
reconstructs feature vector from the attributes,
filters out the images that are not in the 
desired category and returns the 
feature vector matrix and corresponding labels

param: name - pickle containing the array of Imdata with the
              extracted features
       cat - class numbers that we want to include into 
             the output matrix
returns: X - matrix of size (numsamples x feature_vector_size)
         y - corresponding labels to the feature vectors in matrix X
"""
def process_pickle(name, cat=[0,1,2,3,4]):
    dat = pickle.load(open(name, "rb"))
    dat2 = []
    X,y = [],[]

    for i in range(len(dat)):
        # print(dat[i].determine_dataset())
        #print(dat[i].__dict__.keys())
        #print(dat[i].feature_vector)
        #dat[i].reconstruct_feature_vector(['nucleiareas_mean', 'eigratio_mean', 'eigratio_std', 'granularity_mean'])
        dat[i].reconstruct_feature_vector()
        curclass = en[dat[i].determine_class(d)]
        dataset = dat[i].determine_dataset()
        if curclass not in cat or dataset == "only" or dataset == "before" :
            continue
        X.append(dat[i].feature_vector[:])
        #print("a", dat[i].feature_vector.size)
        y.append(curclass)
        dat2.append(dat[i].name)
    return X,y, dat[0], dat2

"""
plots the ys input on y axis and x input on x axis
ys contain lists of y values, each list has a corresponding label 
in the labels
the result is saved into the input file name
param: x : x axis values
       ys : array of arrays, each array has values corresponding to x axis values 
       labels : description of the ys
       name : output plot name
output: plot saved into input file name
"""
def plot_scatter(x, ys, labels, name="noname2", xlabel="C", ticks=None):
    colors = ['blue', 'red', 'green', 'violet']
    plt.figure()
    for i, y in enumerate(ys):
        plt.scatter(x, y, c=colors[i], label=labels[i], s=15)
    plt.legend()
    #plt.yticks(np.arange(0, 1, step=0.1))
    plt.xlabel(xlabel)
    #locs, labs= plt.yticks()  # Get the current locations and labels.
    if ticks == None:
        plt.yticks(np.arange(0, 100.01, step=10))
    #plt.xticks(np.round(np.arange(min(x)-0.001, max(x)+0.001, step=(max(x)-min(x))/10)))
    plt.grid(axis="y")
    plt.ylabel("[%]")
    #plt.ylim(0,1)
    #plt.xticks(np.arange(len(x)), ( *x ))
    #plt.show()
    plt.savefig(name)

"""
prints the input array as the latex table
param: rows - number of rows
       cols - number of columns
       arr - array to be printed
       caption
       label
"""
def print_table_latex(rows, cols, arr, caption="table", label="t1"):
    print("\n")
    print("\\begin{table}[ht!]")
    print("\\centering")
    print("\\begin{tabular}{||" + "c "*cols + "||}")
    print("\\hline")
    print( " & ".join([arr[0][i] for i in range(cols)]) + " \\\\ [0.5ex]")
    print("\\hline\\hline")
    for row in range(1, rows):
        print(" & ".join([str(el) for el in arr[row]]) + " \\\\")
    print("[1ex]")
    print("\\hline")
    print("\\end{tabular}")
    print("\\caption{" + caption + "}")
    print("\\label{" + label + "}")
    print("\\end{table}")

"""
plots the barplot with y input values and input labels
result is saved into input file name
param: labels
       y    - array of individual bar sizes
       name - file name where to save the plot
"""
def plotbar(labels,y, name="bar_occurences.png"):
    y_pos = np.arange(len(labels))
    print(y)
    plt.bar(y_pos, [round(yi*100) for yi in y], align='center', alpha=0.5)
    plt.grid(axis='y')
    plt.xticks(y_pos, labels)
    plt.ylabel('[%]')
    #plt.title('Programming language usage')
    # ax.bar(labels, y)
    # ax.set_ylabel("[%]")
    plt.savefig(name)
    #plt.show()

"""
saves the plot of the train, validation and nn output accuracy
with changing regularization constant C for SVM

param: Cs - the regularization constants to be evaluated
       x_train - feature vector matrix for training set
       y_train - corresponding labels for training set
       x_test - feature vector matrix for test set
       y_test - corresponding class labels for test set
       X - feature vector matrix for nn output set
       y - corresponding class labels for nn output set
       kernel : type of the svm kernel ('poly', 'rbf')

"""
def plot_C(Cs, x_train, y_train,x_test,y_test,X,y, kernel='rbf'):
    acc_train, acc_test, acc_nn = [],[],[]
    for C in Cs:
        clf = make_pipeline(StandardScaler(), SVC(kernel=kernel,gamma='auto', C=C))
        clf.fit(x_train, y_train)

        predicted_training = clf.predict(x_train)
        predicted_testing = clf.predict(x_test)

        acc1, acc2 =accuracy(predicted_training ,y_train), accuracy(predicted_testing ,y_test)
        accnn = accuracy(clf.predict(X) ,y)
        acc_train.append(acc1*100)
        acc_test.append(acc2*100)
        acc_nn.append(accnn*100)
        print("{} accuracy of training prediction is:".format(C), acc1)
        print("{} accuracy of validation prediction is:".format(C), acc2)
        print("{} accuracy of nn prediction is:".format(C), accnn)

    plot_scatter(Cs, [acc_train, acc_test, acc_nn], ["training accuracy", "validation accuracy", "nn output accuracy"],
                 "Ccompared_{}.png".format(nameCscatter))

"""
saves the plot of the train, validation and nn output accuracy
with changing coefficient coef0 in polynomial kernel svm

param: Cs - the regularization constants to be evaluated
       x_train - feature vector matrix for training set
       y_train - corresponding labels for training set
       x_test - feature vector matrix for test set
       y_test - corresponding class labels for test set
       X - feature vector matrix for nn output set
       y - corresponding class labels for nn output set
"""
def plot_C0(coef0, x_train, y_train, x_test,y_test, X,y):
    acc_train, acc_test, acc_nn = [], [], []
    for coef in coef0:
        clf = make_pipeline(StandardScaler(), SVC(kernel='poly', gamma='auto', coef0=coef, C=C))
        clf.fit(x_train, y_train)

        predicted_training = clf.predict(x_train)
        predicted_testing = clf.predict(x_test)

        acc1, acc2 = accuracy(predicted_training, y_train), accuracy(predicted_testing, y_test)
        acc_train.append(acc1*100)
        acc_test.append(acc2*100)
        acc_nn.append(accuracy(clf.predict(X),y)*100)

    plot_scatter(Cs, [acc_train, acc_test, acc_nn], ["training accuracy", "validation accuracy", "nn output accuracy"],
    "c0compared_{}.png".format(nameCscatter), xlabel="coef0")


d, en = create_dict()

# choose the available categories - pairwise, all, or [0,1]
#aval_cat = [[0,1],[0,2],[0,3],[0,4],[1,2],[1,3],[1,4],[2,3],[2,4],[3,4]]
#aval_cat = [[0,1,2,3,4]]
aval_cat = [[0,1]]

pairwise_comparison = {}
split = True
if not split:
    res_stats=  "/home/hanka/PycharmProjects/diplomka/classify/new_features/features_p2p20epL1l10_allref.p"
    nn= "/home/hanka/PycharmProjects/diplomka/classify/new_features/features_p2p20epL1l10_nn.p"
    nameCscatter = "nosplit"
else:
    res_stats= "/home/hanka/PycharmProjects/diplomka/classify/new_features/features_p2p20epL1l10_allref_split.p"
    nn= "/home/hanka/PycharmProjects/diplomka/classify/new_features/features_p2p20epL1l10_nn_split.p"
    nameCscatter="split"

num_features = 15
for cat in aval_cat:
    X, y, dat0, dat2 = process_pickle(res_stats, cat)
    _, _,_, filternames = process_pickle(nn, cat)
    newX, newY = [], []
    for i in range(len(y)):
        if dat2[i] not in filternames:
            newX.append(X[i])
            newY.append(y[i])
    X, y = newX, newY
    print("A",len(X[0]))
    print(len(y))

    testsize = 0.4 * len(y)
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=int(testsize), random_state=4)
    # x_val, x_test, y_val, y_test = train_test_split(x_test, y_test, test_size=int(testsize/2), random_state=4)
    x_train, indices = feature_select(x_train, y_train, num_features)
    x_test = np.array(x_test)
    print("trainset", len(y_train))
    print("valset", len(y_test))

    x_test = x_test[:, indices]
    print("selected features:", dat0.keys_from_indices(indices))

    print(y)
    b = np.array(y)
    counts = [len(b[b == i]) / len(b) for i in range(5)]
    plotbar(["Topotecan", "Daunorubicin", "Etoposide", "DMSO", "No treatment"], counts)
    # Topotecan, Daunorubicin, Etoposide, DMSO,
    Cs = [0.25, 1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220,
          230, 240, 250, 260, 270, 280, 290, 300, 350, 400, 450, 500]  # ,550]#,550,600,650,700]
    Cs = np.linspace(0.01, 20, 50)
    feature_num = [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
    C = 16  # alebo 70 pre split
    coef0 = Cs
    # coef0 = 3.2
    coef1 = 2.5
    acc_train = []
    acc_test = []
    acc_nn = []

    clf = make_pipeline(StandardScaler(), SVC(kernel='rbf',gamma='auto', C=C))
    #clf = make_pipeline(StandardScaler(), SVC(kernel='poly', gamma='auto', coef0=coef1, C=C))
    #
    clf.fit(x_train, y_train)

    acc_trainp, acc_testp = accuracy(clf.predict(x_train), y_train), accuracy(clf.predict(x_test), y_test)
    X, y, _,_ = process_pickle(nn, cat)
    X = np.array(X)
    print(X.shape)
    X = X[:, indices]

    predicted_nn = clf.predict(X)
    accnn = accuracy(predicted_nn, y)
    pairwise_comparison[tuple(cat)] = [round(acc_trainp, 2), round(acc_testp, 2), round(accnn, 2)]
    print("accuracy of training prediction is:", round(acc_trainp, 2))
    # print("accuracy of validation prediction is:", accuracy(clf.predict(x_val), y_val))
    print("accuracy of test prediction is:", round(acc_testp, 2))

    print()
    # X,y,dat2 = process_pickle("features_p2p20epL1l10.p")
    print("accuracy of nn prediction is:", round(accnn, 2))

    print()

    #plot_C(Cs, x_train, y_train, x_test, y_test, X, y,  kernel="rbf")
    #plot_C0(Cs, x_train, y_train, x_test, y_test, X, y, acc_nn=[])


    ####
    # try different number of features, first set num_features to 52 to avoid error due to already filtered features
    ####
    # for C in feature_num:
    #
    #     x_train_s, indices = feature_select(x_train,y_train, C)
    #     clf = make_pipeline(StandardScaler(), SVC(kernel='rbf',gamma='auto', C=1))
    #     clf.fit(x_train_s, y_train)
    #
    #     #print(dat[0].keys_from_indices(indices))
    #
    #     predicted_training = clf.predict(x_train_s)
    #
    #     x_test = np.array(x_test)
    #     x_test_s = x_test[:, indices]
    #     predicted_testing = clf.predict(x_test_s)
    #
    #     acc1, acc2 =accuracy(predicted_training ,y_train), accuracy(predicted_testing ,y_test)
    #     X = np.array(X)
    #     X_s = X[:, indices]
    #     accnn = accuracy(clf.predict(X_s) ,y)
    #     acc_train.append(acc1*100)
    #     acc_test.append(acc2*100)
    #     acc_nn.append(accnn*100)
    #     print("###########################x")
    #     print("{} accuracy of training prediction is:".format(C), acc1)
    #     print("{} accuracy of validation prediction is:".format(C), acc2)
    #     print("{} accuracy of nn prediction is:".format(C), accnn)
    #
    # plot_scatter(feature_num, [acc_train, acc_test, acc_nn],
    #              ["training accuracy", "validation accuracy", "nn output accuracy"],
    #              "featurepairwise_compared_{}.png".format(nameCscatter), xlabel="features number", ticks="default")


translate = {0:"Topotecan", 1:"Daunorubicin", 2:"Etoposide",3:"DMSO" ,4:"No treatment"}
table = [["", "training accuracy", "validation accuracy", "nn output accuracy"]]
for p, item in pairwise_comparison.items():
    row = []
    comparison = translate[p[0]] + " & vs & " + translate[p[1]]
    row.append(comparison)
    row += item

    print(p, item)
    table.append(row)

print(table)
print_table_latex(len(table), len(table[0]), table)

