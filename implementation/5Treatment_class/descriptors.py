"""
helper script containing support methods for Hu-invariants and eigenvalues
"""


import numpy as np

def mipq(posx, posy, p, q):
    # only applicable because all f(x,y) = 1
    xc = np.mean(posx)
    yc = np.mean(posy)
    ysum = np.sum((posy - yc)**q)
    xsum = np.sum((posx - xc)**p)
    return xsum*ysum

def nipq(posx, posy, p, q):
    return mipq(posx, posy, p, q)/ mipq(posx, posy, 0,0)**((p+q)/2 + 1)

"""
computes the Hu invariants from the x and y positions of the
component
param: posx :  x positions belonging to the components
       posy : corresponding y positions belonging to the component
output:
     Hu invariants L23, L24
"""
def Huinvariants(posx, posy):
    L23 = nipq(posx, posy, 2 , 0) + nipq(posx, posy, 0 , 2)
    L24 =  (nipq(posx, posy, 2 , 0) - nipq(posx, posy, 0 , 2))**2 + 4* nipq(posx, posy, 1,1)**2
    return L23, L24

"""
computes the eigenvalues of the component covariance matrix
param: posx : x positions belonging to the components
       posy : corresponding y positions belonging to the component
"""
def eigenv(posx, posy):
    x = np.stack((posx, posy), axis=0)
    covmat = np.cov(x)
    eigval, _ = np.linalg.eig(covmat)
    idx = eigval.argsort()[::-1]
    eigenValues = eigval[idx]

    return eigenValues

