
"""
    script that extracts features from each image and saves the output feature data into the pickle
    usage:
    python extract_features.py  --mask SRC --dest DEST
	SRC : folder with the input image segmentations
	DEST : name of the pickle file where to save results
"""

local = False  # (True to use different file names than the program arguments)

import random
import pickle
import os
import argparse
from skimage import measure as skm
import numpy as np
from numpy.linalg import norm as euklid
from skimage import io
import cv2
import scipy.ndimage as nd

import descriptors
from pprint import pprint

"""
produces the normalized histogram within the given range and number of bins
param: arr - input array 
       bins - number of histogram bins
       minmax - histogram range
return: numpy normalized histogram array 
"""
def myhistogram(arr, bins, minmax):
    mmin, mmax = minmax
    arr[arr<mmin] = mmin
    arr[arr>mmax] = mmax
    return np.histogram(arr, bins, range=(mmin, mmax), density=True)


"""
data structure that holds individual features, image name, and feature vector
"""
class Imdata():
    """
    initializes the features:
    params: name  - name of the image
            nuclei_areas - array of individual component sizes
            border_lengths -array of individual border sizes
            L23s - array of individual components L23 invariants
            L24s - array of individual components L24 invariants
            eig1s - array of individual components larger eigenvalues
            eig2s - array of individual components smaller eigenvalues
            total_area - total area of the components in the image
            bins - number of bins of histograms
            areaminmax - range of the area histogram
            periminmax - range of the perimeter histogram
            ratiominmax -range of the aspect ratio histogram
    init:
        from each of the following: nuclei_areas, border_lengths, L23, L24, eig1s, eig2s
        eig1s/eig2s:
        compute mean, median, standard deviation

        compute perimeter histogram, nuclei_areas histogram, eig1s/eig2s ratio histogram
        construct the feature vector
    """
    def __init__(self,name,  nuclei_areas, border_lengths, L23s, L24s, eig1s, eig2s, total_area, bins=10, areaminmax=None, periminmax=None, ratiosminmax=None):
        granularity = border_lengths/nuclei_areas
        eig1s = 2*eig1s**(1/2)
        eig2s = 2*eig2s**(1/2)
        eigratio = eig2s/eig1s

        # nucleiares_mean, nucleiareas_std = mean_std(nuclei_areas)
        # granularity_mean, granularity_std = mean_std(granularity)
        # L23mean, L23std = mean_std(L23s)
        # L24mean, L24std = mean_std(L24s)
        # eig1mean, eig1std = mean_std(eig1s)
        # eig2mean, eig2std = mean_std(eig2s)
        # eigratio_mean, eigratio_std = mean_std(eigratio)

        nucleiares_mean, nucleiareas_std, nucleiareas_med = mean_std_med(nuclei_areas)
        granularity_mean, granularity_std, granularity_med = mean_std_med(granularity)
        L23mean, L23std, L23med = mean_std_med(L23s)
        L24mean, L24std, L24med = mean_std_med(L24s)
        eig1mean, eig1std, eig1med = mean_std_med(eig1s)
        eig2mean, eig2std, eig2med = mean_std_med(eig2s)
        eigratio_mean, eigratio_std, eigratio_med = mean_std_med(eigratio)

        self.name = name
        self.nucleiareas_mean = nucleiares_mean
        self.nucleiareas_std = nucleiareas_std
        self.granularity_mean = granularity_mean
        self.granularity_std = granularity_std
        self.L23mean = L23mean
        self.L23std = L23std
        self.L24mean = L24mean
        self.L24std = L24std
        self.eig1mean = eig1mean
        self.eig1std = eig1std
        self.eig2mean = eig2mean
        self.eig2std = eig2std
        self.eigratio_mean = eigratio_mean
        self.eigratio_std = eigratio_std
        self.total_area = total_area
        self.area_histogram, _ = myhistogram(nuclei_areas, bins, areaminmax)

        self.nucleiareas_med = nucleiareas_med
        self.granularity_med = granularity_med
        self.L23med= L23med
        self.L24med = L24med
        self.eig1med = eig1med
        self.eig2med = eig2med
        self.eigratio_med = eigratio_med
        self.perim_histogram, _ = myhistogram(nuclei_areas, bins, periminmax)
        self.eigratio_histogram, _ = myhistogram(nuclei_areas, bins, ratiosminmax)

        self.feature_vector = self.construct_feature_vector()

    """
    constructs the feature vector of attributes present in the function
    """
    def construct_feature_vector(self):
        vector = np.array([self.total_area, self.nucleiareas_mean, self.nucleiareas_std, self.granularity_mean,
                           self.granularity_std, self.L23mean, self.L23std, self.L24mean, self.L24std,
                           self.eig1mean, self.eig1std, self.eig2mean, self.eig2std, self.eigratio_mean, self.eigratio_std])
        res = np.concatenate((vector, self.area_histogram))
        return res

    """
    reconstructs the feature vector from the attributes,
    easy to alter and potentially omit some of the features
    """
    def reconstruct_feature_vector(self, names=None):
        if names == None:
            names = list(self.__dict__.keys())
            names.remove("name")
            names.remove("feature_vector")

        vec = np.array([self.__dict__[names[0]]])
        for n in names[1:]:
            to_app = np.array([self.__dict__[n]]) if np.isscalar(self.__dict__[n]) else self.__dict__[n]
            vec = np.concatenate((vec,to_app),0)
        self.feature_vector = vec

    """
    prints the values of the individual attributes
    """
    def print_self(self):
        for key, val in self.__dict__.items():
            print(key, "=", val)

    """
    reconstructs the attributes from the indices of the feature vector
    if the order of features is to change, this function should 
    also change (the histograms have more values in the feature vector
    and thus everything must shift accordingly)
    """
    def keys_from_indices(self, indices):
        keys = list(self.__dict__.keys())
        keys.remove("name")
        keys.remove("feature_vector")
        ret = []
        for idx in indices:
            print(idx, end=" ")
            if idx < 15:
                ret.append(keys[idx])
            elif idx >= 15 and idx <25:
                ret.append(keys[15] + str(idx-15))
            elif idx >= 25:
                new_idx = idx - 9
                if new_idx < 23:
                    ret.append(keys[new_idx])
                elif new_idx >=23 and new_idx < 33:
                    ret.append(keys[-2]+ str(new_idx -23))
                elif new_idx >= 33 and new_idx < 34:
                    ret.append(keys[-1] +str(new_idx -33))

        return ret

    """
    determines the class of the image given the code 
    dictionary
    the dictionary holds the codes for individual classes,
    this function checks if the code is present in the name of the image
    
    param: d - dictionary with the classes and their codes
    """
    def determine_class(self, d):
        treatment = self.name.split("_")[1].split("_")[0]
        # topocean = ["C02", "C03", "C04", "C05"]
        # daunorubicin = ["C14", "C15", "C16", "C17", "D14, D15", "D16", "D17"]
        # etoposide = ["C06", "C07", "C08", "C09", "D06", "D07","D08", "D09"]
        # dmso = ["C18", "C19", "C20", "C21", "D18", "D19", "D20", "D21"]
        # no = ["C10", "C11","C12","C13", "D10","D11","D12","D13", "C22","C23", "D22","D23"]
        # d = {"top":topocean, "dau":daunorubicin, "eto":etoposide, "dmso":dmso, "no":no}
        for key, val in d.items():
            if treatment in val:
                return key
        print("unknown treatment", treatment, self.name)
        return "unknown"

    """
    determines the dataset from the name:
    checks for the occurence of '24h' or '72h' or 'before' or 'only' 
    in the self.name
    """
    def determine_dataset(self):
        if "24h" in self.name:
            return "24h"
        elif "72h" in self.name:
            return "72h"
        elif "before" in self.name:
            return "before"
        elif "only" in self.name:
            return "only"
        else:
            print("unknown dataset", im_name)
            return "unknown"




parser = argparse.ArgumentParser()
parser.add_argument("--mask", default="inputNN_all/masks", help="echo the string you use here")
parser.add_argument("--nnres", default="resultsNN_all/pics_to_vis", help="echo the string you use here")
parser.add_argument("--dest", default="resultsNN_all/evalkvantiv/stats_pix_20.p", help="echo the string you use here")
parser.add_argument("--resfig", default="reseval", help="echo the string you use here")
args = parser.parse_args()


"""
finds the connected components, computes center of each and eliminates those
smaller than 1/10 of mean size of components
param: picture - input binary image
output: array of tuples (each tuple represents one center of an object),
        epsilon : mean of the nuclei minor axis approximation
"""
def find_centers(picture):
    """
    finds the connected components, computes center of each and eliminates those
    smaller than 1/10 of mean size of components
    """
    labeled_image, num_labels = skm.label(picture, connectivity=2, return_num=True)
    sizes = []
    means = []
    for i in range(num_labels):
        res = np.where(labeled_image == (i + 1))
        sizes.append(res[0].size)


    sizes_np = np.array(sizes)
    mean_size = np.mean(sizes_np)
    filtered_sizes = sizes_np[sizes_np> mean_size/10]
    mean_filtered = np.mean(filtered_sizes)
    epsilon = np.sqrt(mean_filtered/np.pi)*2/3
    print("epsilon is: {}".format(epsilon))
    for i in range(num_labels):
        if sizes[i] > mean_size/10:
            res = np.where(labeled_image == (i + 1))

            positions = np.array([np.array([x, y]) for x, y in zip(res[0], res[1])])
            mean = np.mean(positions, axis=0)
            means.append(mean)
    return means, epsilon

"""
computes the sizes of individual components in the image
"""
def component_sizes(labeled_image, num_labels):
    sizes = []
    for i in range(num_labels):
        res = np.where(labeled_image == (i + 1))
        sizes.append(res[0].size)
    return np.array(sizes)


"""
computes the border size of the i-th component in the labeled 
image
"""
def border_size(labeled_image, i, dst, tmpmasknuclei):
    tmpmasknuclei[labeled_image == (i + 1)] = 1
    bordersize = np.sum(tmpmasknuclei * dst)
    tmpmasknuclei[labeled_image == (i + 1)] = 0
    return bordersize

"""
extracts the features from the input image

param: picture - input image
output: features - components_areas, border_lengths, L23s, L24s, eig1s, eig2s, total_area

"""
def find_features(picture):
    labeled_image, num_labels = skm.label(picture, connectivity=2, return_num=True)
    dst = nd.distance_transform_edt(picture)  # distances from 0, in this case from background

    sizes_np = component_sizes(labeled_image, num_labels)
    mean_size = np.mean(sizes_np)

    nuclei_areas = sizes_np[sizes_np > mean_size/10]
    border_lengths, L23s, L24s, eig1s, eig2s = [np.ones((len(nuclei_areas),)) for _ in range(5)]
    total_area = np.sum(nuclei_areas)

    print("extracting features")
    idx = -1
    dst[dst!=1]=0
    tmpmasknuclei = np.zeros((picture.shape))
    for i in range(num_labels):
        if sizes_np[i] > mean_size/10:
            idx += 1
            posx, posy = np.where(labeled_image == (i + 1))
            L23, L24 = descriptors.Huinvariants(posx, posy)
            eig1, eig2 = descriptors.eigenv(posx, posy)

            border_lengths[idx] = border_size(labeled_image, i, dst, tmpmasknuclei)
            L23s[idx], L24s[idx], eig1s[idx], eig2s[idx] = L23, L24, eig1, eig2

    return nuclei_areas, border_lengths, L23s, L24s, eig1s, eig2s, total_area

"""
returns mean and standard deviation of the input array
"""
def mean_std(arr):
    return np.mean(arr), np.std(arr)
"""
returns the mean, standard deviation and median of the input array
"""
def mean_std_med(arr):
    return np.mean(arr), np.std(arr), np.median(arr)

"""
thresholds the image to the binary image
each value of the image smaller than threshold is zero,
while each greater value is one
param: im  - input image
       thresh - value of the threshold
output: binarized image
"""
def binary_transform(im, thresh=0.122):
    im[im > thresh] = 1
    im[im != 1] = 0

"""
mathematical morphology operations:
erosion and dilation are applied to the input image
"""
def closing(im):
    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    min_img = cv2.erode(im, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)
    return dil_img

"""
applies watershed on the image, 
touching nuclei should split

param: im_name -name of the input image
output: image with splitted touching objects
"""
def split_merged_nuclei(im_name):
    img = cv2.imread(im_name)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray[gray > (1 - 0.01)] = 255
    gray[gray < 255] = 0

    # noise removal
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel, iterations=2)
    # sure background area
    sure_bg = cv2.dilate(opening, kernel, iterations=3)
    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
    ret, sure_fg = cv2.threshold(dist_transform, 0.7 * dist_transform.max(), 255, 0)
    # Finding unknown region
    sure_fg = np.uint8(sure_fg)
    unknown = cv2.subtract(sure_bg, sure_fg)
    # Marker labelling
    ret, markers = cv2.connectedComponents(sure_fg)
    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1
    # Now, mark the region of unknown with zero
    markers[unknown == 255] = 0
    markers = cv2.watershed(img, markers)
    gray[markers == -1] = 0
    return gray

"""
extracts the features from the input images,
if split is True, it also applies watershed

param: im_name  - name of the image to be processed
       split    - True for applying the watershed
output:
     vector of features defined in the find_features
"""
def process_image(im_name, split):
    if split=="split":
        im = split_merged_nuclei(im_name)
        print("splitted")
    else:
        im = io.imread(im_name)

    im = im / np.amax(im)
    binary_transform(im)
    im = closing(im)
    print("imshape", im.shape)

    return find_features(im)

"""
prints the attributes with values for the object
"""
def dump(obj):
  obj.print_self()
  # for attr in dir(obj):
  #     if attr == "__dict__":
  #         for key, val in getattr(obj, attr).items():
  #             print(key, "=", val)
    #print("obj.%s = %r" % (attr, getattr(obj, attr)))



if not local:
    masksdir = args.mask
    resdir = args.nnres
    res_stats = args.dest
    resfig = args.resfig
else:
    masksdir = "../neuronka_eval/masks"
    resdir = "res20epL1_Adam_l10"
    res_stats = "none.p" #stats/statsdst.p"
    resfig="split"#, # "resevaldst_FPs" #"resevaldst"

########################################################################
# process the files in the input directory and create feature vectors  #
########################################################################
if __name__=="__main__":
    masks_file_names = sorted([os.path.join(masksdir, mname) for mname in os.listdir(masksdir)])
    print(masks_file_names)

    # nuclei_areas = []
    # perimeters = []
    # aspect_ratios = []

    imdatas_NN = []
    i = 0
    for im_name in masks_file_names:
        print(i, "/", len(masks_file_names), im_name)
        i += 1

        featuresim = process_image(im_name, split=resfig)
        #nuclei_areas, border_lengths, L23s, L24s, eig1s, eig2s, total_area
        # nuclei_areas.append(featuresim[0])
        # perimeters.append(featuresim[1])
        # aspect_ratios.append(featuresim[4]/featuresim[5])
        print(im_name)

        imdataNN = Imdata(im_name.split("/")[-1],  *featuresim, areaminmax=(306, 11924), periminmax=(50.0, 784.0), ratiosminmax=(1.0713,37.9643))
        imdatas_NN.append(imdataNN)



    # nuclei_areas_min, nuclei_areas_max = min([np.min(arr) for arr in nuclei_areas]), max([np.max(arr) for arr in nuclei_areas])
    # perimeters_min, perimeters_max = min([np.min(arr) for arr in perimeters]), max([np.max(arr) for arr in perimeters])
    # aspect_ratios_min, aspect_ratios_max =  min([np.min(arr) for arr in aspect_ratios]), max([np.max(arr) for arr in aspect_ratios])
    #
    # print("nucleiarea min, max:", nuclei_areas_min, nuclei_areas_max)
    # print("perimeters min, max:", perimeters_min, perimeters_max)
    # print("aspect_ratios min, max:", aspect_ratios_min, aspect_ratios_max)

    pickle.dump(imdatas_NN, open(res_stats, "wb"))
    dat = pickle.load(open(res_stats, "rb"))
    dump(dat[0])
    print(dat[0].name)






