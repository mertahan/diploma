"""
script that processes the output log of the pix2pix neural network and plots the log progress throughout epochs

usage :
	python losses_from_log.py --filename NAME
	NAME : name of the nn output log (prints of the nn training and testing)
	output: ave_gmov.png" (plot with the generator training loss),
		"ave_dmov.png" (plot with the discriminator training loss),
		"ave_gmovval.png" (plot with the generator validation loss),
		"ave_dmovval.png" (plot with the discriminator validation loss)

"""


import numpy as np
import itertools
from matplotlib import  pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--filename", default="resnn.out", help="name of file with output logs")
args = parser.parse_args()
#CRIT = args.crit

filenames = [args.filename]

filenames=[
    #"20epL1Adam_l10Forbid.out",
    #"20epL1Adam_l10Forbid8ep.out",

    "/home/hanka/PycharmProjects/diplomka/pix2pixUnet/res20epL1_Adam_l10_ep0.out"

]

filenames_val=[
    "/home/hanka/PycharmProjects/diplomka/pix2pixUnet/early_stop20epL1_Adam_l10_forw.out"
]


"""
extracts the losses from the filenames (output log of the neural network)
"""
def losses_from_file(filenames):
    glosses = [[] for _ in range(20)]
    dlosses = [[] for _ in range(20)]
    ep = 0
    for fname in filenames:
        with open(fname) as openfileobject:
            print(fname)
            for line in openfileobject:
                if "D loss" in line:
                    cats = line.split("]")
                    epoch = int(cats[0].split("/")[0].split(" ")[-1])
                    dloss = cats[2].split(": ")[-1]
                    gloss = cats[3].split(": ")[1].split(",")[0]
                    if gloss == "nan" or dloss == "nan":
                        continue
                    else:
                        if ep == epoch:
                            print("epoch {} found".format(ep))
                            ep += 1
                        print(line)
                        glosses[epoch].append(float(gloss))
                        dlosses[epoch].append(float(dloss))
    return glosses, dlosses

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

"""
plots the ys input on y axis and x input on x axis
ys contain lists of y values, each list has a corresponding label 
in the labels
the result is saved into the input file name
param: x : x axis values
       ys : array of arrays, each array has values corresponding to x axis values 
       labels : description of the ys
       name : output plot name
output: plot saved into input file name
"""
def plot_scatter(x, ys, labels, name="noname2"):
    colors = ['blue', 'red', 'green', 'violet']
    plt.figure()
    for i, y in enumerate(ys):
        plt.scatter(x, y, c=colors[i], label=labels[i], s=11.5)
    plt.legend(fontsize=13)
    plt.xlabel("epochs", fontsize=15)
    plt.ylabel("loss", fontsize=15)
    plt.grid(axis='y')
    plt.xticks(range(0, int(max(x))+1, 1))

    mmin, mmax = np.min(ys), np.max(ys)

    plt.yticks(np.round(np.arange(mmin-1, mmax+2, (mmax-mmin)/9), 2))
    #plt.xticks(np.arange(len(x)), ( *x ))
    #plt.show()
    plt.savefig(name)

"""
plots the averaged loss throughout epochs
"""
def plot_average_plot(averagedGmov, name, label="no"):
    plt.figure()
    plt.plot(averagedGmov, label=label)
    if label != "no":
        plt.legend(fontsize=13)
    mmin, mmax = np.min(averagedGmov), np.max(averagedGmov)
    plt.yticks(np.round(np.arange(mmin -mmin/20, mmax +mmax/20 , (mmax - mmin) / 15), 2))
    plt.grid(axis='y')
    plt.xlabel("evaluated samples throughout epochs", fontsize=14)
    plt.ylabel("loss", fontsize=15)
    plt.savefig(name)


glosses, dlosses = losses_from_file(filenames)

print(glosses)
print(dlosses)

allG = np.array(list(itertools.chain(*glosses)))
allD = np.array(list(itertools.chain(*dlosses)))

print(allG)
for ep in glosses:
    print(ep)
averagedG= [np.mean(np.array(ep)) for ep in glosses]
averagedD= [np.mean(np.array(ep)) for ep in dlosses]
averagedGmov= moving_average(allG, 500)
averagedDmov= moving_average(allD, 500)

glossesval, dlossesval = losses_from_file(filenames_val)
for ep in glosses:
    print(ep)
averagedGval= [np.mean(np.array(ep)) for ep in glossesval]
averagedDval= [np.mean(np.array(ep)) for ep in dlossesval]

allGval = np.array(list(itertools.chain(*glossesval)))
allDval = np.array(list(itertools.chain(*dlossesval)))
averagedGvalmov= moving_average(allGval, 250)
averagedDvalmov= moving_average(allDval, 250)

fig, axs = plt.subplots(2)
fig.suptitle('Vertically stacked subplots')
axs[0].plot(averagedGvalmov)
axs[1].plot(averagedDvalmov)
plt.show()

# fig, axs = plt.subplots(2)
# fig.suptitle('Vertically stacked subplots')
# axs[0].plot(averagedGmov)
# axs[1].plot(averagedDmov)
plot_average_plot(averagedGmov, "ave_gmov.png", label="generator training loss")
plot_average_plot(averagedDmov, "ave_dmov.png", label="discriminator training loss")

plot_average_plot(averagedGvalmov, "ave_gmovval.png", label="generator validation loss")
plot_average_plot(averagedDvalmov, "ave_dmovval.png", label="discriminator validation loss")
#plt.plot(averagedG)
#plt.plot(averagedD)
print(len(averagedG))

plot_scatter([i+1 for i in range(20)], [ averagedGval], ["validation"], "epochs_val.png")
plot_scatter([i+1 for i in range(20)], [ averagedG], ["training"], "epochs_train.png")

