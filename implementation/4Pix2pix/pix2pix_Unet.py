"""

skeleton implementation from:
https://github.com/eriklindernoren/PyTorch-GAN/tree/master/implementations/pix2pix
licence:
https://github.com/eriklindernoren/PyTorch-GAN/blob/master/LICENSE
modified :29.4.2021

script that loads the dataset, trains the UNet network and tests the network on the test set
	usage:
		python pix2pixUnet.py -- epoch STARTEPOCH --crit CRITERIUM --optim OPTIMIZER 
			--lam LAMBDA --fnames NAMES --w WEIGHT --lr LEARNING_RATE --decay_epoch DEC
			--n_cpu CPU --channels NCHANNELS --src SRC --dest DEST
			--only_names TAG --val_percent PERCENT --epochs EPOCHS --minibatchsize BSIZE
			--slice_size SLICE --stride_size STRIDE --H HEIGHT --W WEIGHT
	
		STARTEPOCH : epoch  where to continue training, 0 if from beggining
		CRITERIUM : loss criterium (BCE or L1)
		OPTIMIZER : Adam or RMS
		LAM : lambda for generator criterium weighing
		NAMES : pickled list of names not to include into training and testing
		WEIGHT : back (for greater punishment for FP) or for (for greater punishment for FN) or no (for equal weights)
		LEARNING_RATE : learning rate of oprimizer
		DEC : epoch number from which to start lr decay
		CPU : number of cpus
		NCHANNELS : number of image channels
		SRC : folder with masks and contrast same as in UNet
		DEST: folder where to save results
		TAG: YES for loading names from list of names instead of contrast folder
		PERCENT: percentage of dataset used for testing
		EPOCHS : number of epochs
		BSIZE : number of slices of the pictures are processed at once
		SLICE : size of the cut square from the picture
		STRIDE : size of the stride of cutting window
		HEIGHT : desired height of scaled input image
		WIDTH : desired width of scaled input image

	short usage:
    python pix2pixUnet.py --src SRC --dest DEST

    to run on grid set GRID to True

    note: don't use net.eval() if the batch size is too small

"""

import argparse
import os
import numpy as np
import math
import itertools
import time
import datetime
import sys

import torchvision.transforms as transforms
from torchvision.utils import save_image

from torch.utils.data import DataLoader, random_split
from torchvision import datasets
from torch.autograd import Variable

from models import *
# from datasets import *

import torch.nn as nn
import torch.nn.functional as F
import pickle
import transf
from loader import SegDataset
import torch
from skimage import io
from model import UNet
import torch.nn.functional as f

from torch.utils.data.sampler import SubsetRandomSampler
from scipy import signal
from matplotlib import pyplot as plt

GRID = True  # (True to use different file names than the program arguments)

REBUILD = True   # (False to load the already built train set and test set in the opt.dest)

parser = argparse.ArgumentParser()
parser.add_argument("--epoch", type=int, default=0, help="epoch to start training from")
parser.add_argument("--crit", default="L1", help="BCE for binary cross entropy, DST for distance transform, L1")
parser.add_argument("--optim", default="Adam", help="Adam or RMS")
parser.add_argument("--lam", type=float, default=10, help="lambda generator criterium")
parser.add_argument("--fnames", default="forbidnames.p", help="frobidden names for trainset")
parser.add_argument("--w", default="no", help="back, for, no weighing of L1 loss")

parser.add_argument("--lr", type=float, default=0.0002, help="adam: learning rate")
parser.add_argument("--b1", type=float, default=0.5, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of first order momentum of gradient")
parser.add_argument("--decay_epoch", type=int, default=100, help="epoch from which to start lr decay")
parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
parser.add_argument("--channels", type=int, default=1, help="number of image channels")
parser.add_argument(
    "--sample_interval", type=int, default=1, help="interval between sampling of images from generators"
)

parser.add_argument("--checkpoint_interval", type=int, default=1, help="interval between model checkpoints")
parser.add_argument("--src", default=".", help="directory with subdirectories: contrast, masks")
parser.add_argument("--dest", default="/home/hanka/PycharmProjects/diplomka/pix2pixUnet/results",
                    help="directory to save validation pictures")

parser.add_argument("--only_names", default="YES", help="YES to load contrast image names from pickle file")
parser.add_argument("--val_percent", type=float, default=0.3,
                    help=" percentage of the dataset to be used as validation set")
parser.add_argument("--epochs", type=int, default=20, help="number of epochs")
parser.add_argument("--minibatchsize", type=int, default=4,
                    help="how many slices of the pictures are processed at once")
parser.add_argument("--slice_size", type=int, default=560, help="size of the cut square from the picture")
parser.add_argument("--stride_size", type=int, default=400, help="size of the stride of cutting window")
parser.add_argument("--H", type=int, default=2160, help="desired height of scaled input image")
parser.add_argument("--W", type=int, default=2560, help="desired width of scaled input image")

opt = parser.parse_args()

ONLY_NAMES = True if opt.only_names == "YES" else False
VAL_PERCENT = opt.val_percent
EPOCHS = opt.epochs
BATCH_SIZE = 1  # size of the batch, how many pictures are processed at once
MINIBATCH_SIZE = opt.minibatchsize
SLICE_SIZE = opt.slice_size
STRIDE_SIZE = opt.stride_size
H = opt.H
W = opt.W

if not GRID:
    ONLY_NAMES = False
    EPOCHS = 3
    MINIBATCH_SIZE = 2
    SLICE_SIZE = 300
    STRIDE_SIZE = 100
    H = 400
    W = 400

if (H - SLICE_SIZE) % STRIDE_SIZE != 0 or (W - SLICE_SIZE) % STRIDE_SIZE != 0:
    print("stride must divide height and width of the resized picture")

if not os.path.isdir(opt.dest):
    os.system("mkdir {}".format(opt.dest))
if not os.path.isdir("{}/saved_models".format(opt.dest)):
    os.system("mkdir {}/saved_models".format(opt.dest))
print(opt)
print("running on grid:", GRID)
dir_images = os.path.join(opt.src, "contrast")
# dir_images = os.path.join(opt.src, "masks")
image_names = os.path.join(opt.src, "contrast.p")
dir_masks = os.path.join(opt.src, "masks")

CRIT = opt.crit
device = "cuda" if torch.cuda.is_available() else "cpu"
# device = "cpu" #cuda" if torch.cuda.is_available() else "cpu"
print(device)


"""
saves the checkpoint after the training epoch
checkpoint contains the epoch number, net, and optimizer
param: epoch - epoch number
       net   - neural network to save
       optimizer - current optimizer
"""
def save_checkpoint(epoch, net, optimizer, filename='checkpoint.pth'):
    print("saving checkpoint after epoch: {}".format(epoch), filename)
    checkpoint = {
        'epoch': epoch,
        'net': net,
        'optimizer': optimizer}
    torch.save(checkpoint, filename)

"""
loads the checkpoint from the file
"""
def load_checkpoint(filename='checkpoint.pth'):
    print("loading", filename)
    checkpoint = torch.load(filename)
    epoch = checkpoint['epoch']
    net = checkpoint['net']
    optimizer = checkpoint['optimizer']
    print("loaded checkpoint, epoch {} prepaired".format(epoch + 1), filename)
    return epoch, net, optimizer


""" saves the network net into the filename"""
def save_net(filename, net):
    torch.save(net.state_dict(), filename)

"""
applies the sigmoid on the mask prediciton
"""
def sigmoidnorm(mask_pred):
    pred = torch.sigmoid(mask_pred)
    return pred


""" 
    counts the normalization factor for the loss function as the number of the background pixels devided by
    the number of the foreground pixels
    the normalization factor is computed across whole dataset

    returns the tensor of size of the picture containing the normalization factor everywhere
"""
def count_norm_factor(trainloader, picture_size):
    background_size = 1
    foreground_size = 1
    i = 0
    for sample in trainloader.dataset:
        i += 1
        img_con, img_label = sample[:2]
        background_size += len(img_label[img_label <= 0.5])
        foreground_size += len(img_label[img_label > 0.5])
        if i > 10:
            break

    pos_weight = torch.ones([picture_size]) * background_size / foreground_size  # All weights are equal to 1
    return pos_weight, background_size / foreground_size

"""
splits the tiles of the image into multiple minibatches so that the 
neural network does not collapse on the memory limit
"""
def split_batch(x, step):
    splitted = []
    for i in range(0, x.shape[0], step):
        if len(x.shape) == 4:
            a = x[i:i + step, :, :, :]
        elif len(x.shape) == 3:
            a = x[i:i + step, :, :]
        else:
            print("wrong splitting, expected dim 3 or 4, got {}".format(len(x.shape)))
        splitted.append(a)
    return splitted

"""Returns a 2D Gaussian kernel array."""
def gkern(kernlen=560, std=140):
    """Returns a 2D Gaussian kernel array."""
    gkern1d = signal.gaussian(kernlen, std=std).reshape(kernlen, 1)
    gkern2d = np.outer(gkern1d, gkern1d)
    return gkern2d

"""
folds the gaussian weighted images (smooth transition between tiles)
"""
def fold_sliced_images(p, N, C, H, W, SLICE_SIZE, STRIDE_SIZE, patches_shape):
    pc = p.cpu()
    kern = gkern(SLICE_SIZE)
    patches = pc.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)
    pp = patches.shape[0]
    folded = np.zeros((H, W))
    folded_weights = np.zeros((H, W))
    sr, sc = 0, 0

    for i in range(pp):
        folded[sr:sr + SLICE_SIZE, sc:sc + SLICE_SIZE] = folded[sr:sr + SLICE_SIZE,
                                                         sc:sc + SLICE_SIZE] + kern * patches[i, 0, :, :].numpy()
        folded_weights[sr:sr + SLICE_SIZE, sc:sc + SLICE_SIZE] = folded_weights[sr:sr + SLICE_SIZE,
                                                                 sc:sc + SLICE_SIZE] + kern
        sc += STRIDE_SIZE
        if sc + SLICE_SIZE > W:
            sc = 0
            sr += STRIDE_SIZE

    folded = folded / folded_weights
    folded = torch.from_numpy(folded)
    folded1 = torch.reshape(folded, (1, 1, *folded.shape[:]))  # folded.resize(1,1, *folded.shape[:])
    return folded1

"""
folds the images, the overlaps are averaged
"""
def fold_sliced_images_old(p, N, C, H, W, SLICE_SIZE, STRIDE_SIZE, patches_shape):
    patches = p.reshape(patches_shape)
    patches = patches.reshape(N, C, (H - SLICE_SIZE) // STRIDE_SIZE + 1, (W - SLICE_SIZE) // STRIDE_SIZE + 1,
                              SLICE_SIZE * SLICE_SIZE)
    patches = patches.permute(0, 1, 4, 2, 3)
    patches = patches.squeeze(1)
    patches = patches.view(N, SLICE_SIZE * SLICE_SIZE, -1)
    folded = f.fold(patches, (H, W), kernel_size=SLICE_SIZE, stride=STRIDE_SIZE)
    counter = f.fold(torch.ones_like(patches), (H, W), kernel_size=(SLICE_SIZE, SLICE_SIZE), stride=STRIDE_SIZE)
    folded = folded / counter
    return folded


"""
    saves the im1 and im2 concatenated horizontally with the green barrier between them
"""
def concat_images(filename, im1, im2):
    im1_rgb = np.stack([im1, im1, im1], axis=2)
    im1_rgb = im1_rgb - np.min(im1_rgb)
    im1_rgb = im1_rgb / np.amax(im1_rgb)
    im2_rgb = np.stack([im2, im2, im2], axis=2)
    im2_rgb = im2_rgb - np.min(im2_rgb)
    im2_rgb = im2_rgb / np.amax(im2_rgb)
    border = np.ones([im1.shape[0], 10])  # *255
    border_rgb = np.stack([border * 0, border, border * 0], axis=2)
    res = np.concatenate((im1_rgb, border_rgb, im2_rgb), axis=1)
    res = res * 255
    io.imsave("{}/{}".format(opt.dest, filename), res.astype(np.uint8))


"""
neural network testing
param: net : the neural network
       val_loader : Dataloader with the test set images
       device : "CPU" or "GPU"
       H_original : height of the original image for reconstruction
       W_original : width of the original image for reconstruction
output:
       saves the network output reconstruction into the destination specified in
       args.dest
       it also saves the concatenated image where the NN output (left) and mask (right)
       are side by side

"""
def try_test(net, val_loader, device, H_original, W_original):
    # net.eval()
    ind = -1
    forbidind = []
    #forbidind = pickle.load(open("test_forbid_indices.p", "rb"))

    for batch in val_loader:
        ind += 1
        if ind in forbidind:
            continue

        inputs, labels, name = batch

        inp_shape = inputs.shape

        inputs = inputs.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)
        labels = labels.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)

        with torch.no_grad():
            split_inputs = split_batch(inputs, MINIBATCH_SIZE)
            outputs = []
            for inputs in split_inputs:
                inputs = inputs.to(device)
                inputs = inputs.float()
                output = net(inputs)
                outputs.append(output)
            mask_pred = torch.cat(outputs, dim=0)

            # reconstruct original image
            N, C = inp_shape[:2]
            mask_pred = fold_sliced_images(mask_pred, N, C, H_original, W_original, SLICE_SIZE, STRIDE_SIZE, inp_shape)
            labels = fold_sliced_images(labels, N, C, H_original, W_original, SLICE_SIZE, STRIDE_SIZE, inp_shape)

            if CRIT == "DST":
                pred = mask_pred.float()
                pred = (pred - torch.min(pred)) / torch.max(pred)
                labels = labels.float()
                lab = (labels - torch.min(labels)) / torch.max(labels)
            else:
                pred = torch.sigmoid(mask_pred)
                pred[pred > 0.5] = 1
                pred[pred <= 0.5] = 0
                lab = labels

            if pred.shape[0] <= 0:
                continue
            pred2D = pred[0, 0, :, :]
            lab2D = lab[0, 0, :, :]
            toSave = (pred2D.cpu() * 255).numpy()
            io.imsave("{}/{}".format(opt.dest, name[0]), toSave.astype(np.uint8))
            concat_images("{}_combined.png".format(name[0][:-4]), pred2D.cpu() * 255, lab2D.cpu() * 255)


names_images = pickle.load(open(image_names, "rb")) if ONLY_NAMES else "local"
if CRIT == "DST":
    transformations = [transf.Rescale((H, W)), transf.DistTransformLabels(), transf.ToTensor(),
                       transf.Slices(SLICE_SIZE, STRIDE_SIZE)]
else:
    transformations = [transf.Rescale((H, W)), transf.ToTensor(), transf.Slices(SLICE_SIZE, STRIDE_SIZE)]
dataset = SegDataset(dir_images, dir_masks, names_images, transforms=transforms.Compose(transformations),
                     only_names=ONLY_NAMES)

# split the dataset to the train set and the test set
if REBUILD:
    n_val = int(len(dataset) * VAL_PERCENT)
    n_train = len(dataset) - n_val
    train, val = random_split(dataset, [n_train, n_val])
    train_loader = DataLoader(train, batch_size=BATCH_SIZE, shuffle=True, num_workers=1)
    val_loader = DataLoader(val, batch_size=BATCH_SIZE, shuffle=False, num_workers=1)
else:
    train_loader = pickle.load(open("{}/trainloader.p".format(opt.dest), "rb"))
    val_loader = pickle.load(open("{}/valloader.p".format(opt.dest), "rb"))

pickle.dump(train_loader, open("{}/trainloader.p".format(opt.dest), "wb"))
pickle.dump(val_loader, open("{}/valloader.p".format(opt.dest), "wb"))
print("data loaded")

pos_weight, w = count_norm_factor(train_loader, SLICE_SIZE)

# Loss functions
criterion_GAN = torch.nn.MSELoss()
if CRIT == "L1":
    print("criterium L1")
    criterion_pixelwise = torch.nn.L1Loss()
elif CRIT == "BCE":
    criterion_pixelwise = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight.to(device))
else:
    criterion_pixelwise = torch.nn.MSELoss()

# Loss weight of L1 pixel-wise loss between translated image and real image
lambda_pixel = opt.lam

# Initialize generator and discriminator
generator = UNet(n_channels=1, n_classes=1)  # GeneratorUNet()
discriminator = Discriminator()

if opt.epoch != 0:
    # Load pretrained models
    ep, generator, optimizer_G = load_checkpoint("{}/saved_models/generator_{}.pth".format(opt.dest, opt.epoch))
    ep, discriminator, optimizer_D = load_checkpoint("{}/saved_models/discriminator_{}.pth".format(opt.dest, opt.epoch))
else:
    # Initialize weights
    # generator.apply(weights_init_normal)
    discriminator.apply(weights_init_normal)
    # Optimizers #
    lr = 0.01
    if opt.optim == "Adam":
        print("Ad")
        optimizer_G = torch.optim.Adam(generator.parameters(), lr=opt.lr, betas=(opt.b1, opt.b2))
        optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=opt.lr, betas=(opt.b1, opt.b2))
    else:
        optimizer_G = torch.optim.RMSprop(generator.parameters(), lr=lr, weight_decay=1e-8, momentum=0.9)
        optimizer_D = torch.optim.RMSprop(discriminator.parameters(), lr=lr, weight_decay=1e-8, momentum=0.9)

if device == "cuda":
    generator = generator.to(device)
    discriminator = discriminator.to(device)
    criterion_GAN = criterion_GAN.to(device)
    criterion_pixelwise = criterion_pixelwise.to(device)
print("done cuda")

# Tensor type
Tensor = torch.cuda.FloatTensor if device == "cuda" else torch.FloatTensor

# ----------
#  Training
# ----------

# Calculate output of image discriminator (PatchGAN)
patch = (1, SLICE_SIZE // 2 ** 4, SLICE_SIZE // 2 ** 4)
print("Starting training")
prev_time = time.time()

for epoch in range(opt.epoch, EPOCHS):
    for i, data in enumerate(train_loader):
        inputs, labels, name = data
        inp_shape = inputs.shape
        inputs = inputs.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)
        labels = labels.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)

        split_inputs = split_batch(inputs, MINIBATCH_SIZE)
        split_labels = split_batch(labels, MINIBATCH_SIZE)

        for inputs, labels in zip(split_inputs, split_labels):
            labels = labels.double().to(device)
            inputs = inputs.to(device)
            inputs = inputs.float()

            # Adversarial ground truths
            valid_ground = Variable(Tensor(np.ones((labels.size(0), *patch))), requires_grad=False)
            fake_ground = Variable(Tensor(np.zeros((labels.size(0), *patch))), requires_grad=False)

            # ------------------
            #  Train Generators
            # ------------------

            optimizer_G.zero_grad()

            # GAN loss
            fake_gen = generator(inputs)
            if CRIT == "L1":
                fake_gen = sigmoidnorm(fake_gen)

            pred_fake = discriminator(fake_gen, inputs)  # compares labels from generator with desired labels

            loss_GAN = criterion_GAN(pred_fake, valid_ground)

            if CRIT == "L1":
                loss_pixel_f = criterion_pixelwise(fake_gen[labels > 0.5], labels[labels > 0.5])
                loss_pixel_b = criterion_pixelwise(fake_gen[labels < 0.5], labels[labels < 0.5])
                if opt.w == "back":
                    loss_pixel = loss_pixel_f + loss_pixel_b * w
                elif opt.w == "for":
                    loss_pixel = loss_pixel_f * w + loss_pixel_b
                elif opt.w == "no":
                    loss_pixel = loss_pixel_f + loss_pixel_b
            else:
                loss_pixel = criterion_pixelwise(fake_gen, labels.float())

            # Total loss
            loss_G = loss_GAN + lambda_pixel * loss_pixel

            loss_G.backward()

            optimizer_G.step()

            # ---------------------
            #  Train Discriminator
            # ---------------------

            optimizer_D.zero_grad()

            # Real loss
            pred_real = discriminator(labels.float(), inputs)
            loss_real = criterion_GAN(pred_real, valid_ground)

            # Fake loss
            pred_fake = discriminator(fake_gen.detach(), inputs)
            loss_fake = criterion_GAN(pred_fake, fake_ground)

            # Total loss
            loss_D = 0.5 * (loss_real + loss_fake)

            loss_D.backward()
            optimizer_D.step()

            # --------------
            #  Log Progress
            # --------------

        # Determine approximate time left
        batches_done = epoch * len(train_loader) + i
        batches_left = EPOCHS * len(train_loader) - batches_done
        time_left = datetime.timedelta(seconds=batches_left * (time.time() - prev_time))
        prev_time = time.time()

        # Print log
        sys.stdout.write(
            "\r[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f, pixel: %f, adv: %f] ETA: %s"
            % (
                epoch,
                EPOCHS,
                i,
                len(train_loader),
                loss_D.item(),
                loss_G.item(),
                loss_pixel.item(),
                loss_GAN.item(),
                time_left,
            )
        )
        print()

    if opt.checkpoint_interval != -1 and epoch % opt.checkpoint_interval == 0:
        # Save model checkpoints
        save_checkpoint(epoch, generator, optimizer_G, "{}/saved_models/generator_{}.pth".format(opt.dest, epoch + 1))
        save_checkpoint(epoch, discriminator, optimizer_D,
                        "{}/saved_models/discriminator_{}.pth".format(opt.dest, epoch + 1))

save_net("{}/saved_models/generator.pt".format(opt.dest), generator)
print("saved final net:", "{}/saved_models/generator.pt".format(opt.dest))

# generator.eval()
try_test(generator, val_loader, device, H, W)
