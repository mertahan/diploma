"""
skeleton implementation in github:
https://github.com/milesial/Pytorch-UNet
modified:
29.4.2021
licence:
https://github.com/milesial/Pytorch-UNet/blob/master/LICENSE


    script that loads the dataset, trains the UNet network and tests the network on the test set
	usage:
		python main.py --src SRCDIR --dest RESDIR
				--only_names TAG --val_percent PERCENT --epochs EPOCHS --minibatchsize BSIZE
				--slice_size SLICE --stride_size STRIDE --H HEIGHT --W WEIGHT --CRIT CRIT

		SRCDIR = directory containing the subdirectories "masks" and "contrast" (or pickle file "contrast.p"
			instead of directory "contrast")
			both subdirectories contain the images that correspond to each other after sorting
			if using "contrast.p" insted of "contrast" directory, "contrast.p" contains the names of contrast images
		RESDIR = directory where the output segmentations are saved
			for each image in the test set the script creates the segmentation image of same name,
			and the segmentation compared with a reference in one image named $input_image_name$_combined
		TAG: YES for loading names from list of names instead of contrast folder
		PERCENT: percentage of dataset used for testing
		EPOCHS : number of epochs
		BSIZE : number of slices of the pictures are processed at once
		SLICE : size of the cut square from the picture
		STRIDE : size of the stride of cutting window
		HEIGHT : desired height of scaled input image
		WIDTH : desired width of scaled input image
		CRIT : criterium, either "DST" (for distance transform based loss) or "PIX_WISE" (for pixelwise based loss)

	short usage:
		python main.py --src SRCDIR --dest RESDIR

	to run on grid set GRID to True
	note: don't use net.eval() if the batch size is too small



"""


GRID = True  # (False to use different settings than loaded in the program arguments)


from loader import SegDataset
from torch.utils.data import DataLoader, random_split
from model import UNet
import torch.optim as optim
import torch
import torch.nn.functional as f
from torch import nn
import transf
from torchvision import transforms, utils
import pickle
import dice_coef
from torch.utils.data.sampler import SubsetRandomSampler
from scipy import signal

# from torch.utils.tensorboard import SummaryWriter
if GRID:
    import matplotlib
    matplotlib.use('Agg')

from matplotlib import pyplot as plt
import numpy as np
import os
import argparse
from skimage import io
import time

start = time.time()



parser = argparse.ArgumentParser()
parser.add_argument("--src", default=".", help="directory with subdirectories: contrast, masks")
parser.add_argument("--dest", default="/home/hanka/PycharmProjects/diplomka/neuronka/results2",
                    help="directory to save validation pictures")
#parser.add_argument("--crit", default="DST", help="criterium NN")

parser.add_argument("--only_names", default="YES", help="YES to load contrast image names from pickle file")
parser.add_argument("--val_percent", type=float, default=0.3, help=" percentage of the dataset to be used as validation set")
parser.add_argument("--epochs", type=int, default=20, help="number of epochs")
parser.add_argument("--minibatchsize", type=int, default=4, help="how many slices of the pictures are processed at once")
parser.add_argument("--slice_size", type=int, default=560, help="size of the cut square from the picture")
parser.add_argument("--stride_size", type=int, default=400, help="size of the stride of cutting window")
parser.add_argument("--H",type=int, default=2160,  help="desired height of scaled input image")
parser.add_argument("--W",type=int, default=2560, help="desired width of scaled input image")
parser.add_argument("--CRIT", default="DST", help="fitting either 'DST' for distance from background or 'PIX_WISE' for pixel wise segm")

args = parser.parse_args()


ONLY_NAMES = True if args.only_names == "YES" else False
VAL_PERCENT = args.val_percent
EPOCHS= args.epochs
BATCH_SIZE = 1      # size of the batch, how many pictures are processed at once
MINIBATCH_SIZE =  args.minibatchsize
SLICE_SIZE= args.slice_size
STRIDE_SIZE = args.stride_size
H = args.H
W = args.W
CRIT = args.CRIT  # fitting either "DST" for distance from background or "PIX_WISE" for pixel wise segm


SAVE_NET = True  # set to True to save net after training
PRETRAINED = "full"  # set to "none" if no pretraining, "full" if the net is fully pretrained, "semi" if needed to continue learning
REBUILD_DATA = True  # make sure to include forbidnames.p and trainames.p in srcdir if False
device = "cuda" if torch.cuda.is_available() else "cpu"
print(device)

if not GRID:
    print("running local")
    ONLY_NAMES = False
    EPOCHS = 1
    MINIBATCH_SIZE = 2
    SLICE_SIZE = 200
    STRIDE_SIZE = 100
    H = 300
    W = 300

if (H - SLICE_SIZE) % STRIDE_SIZE != 0 or (W - SLICE_SIZE) % STRIDE_SIZE != 0:
    print("stride must divide height and width of the resized picture")

"""
finds the last created .pth file
"""
def latest_file_pth(path):
    files = os.listdir(path)
    paths = [os.path.join(path, basename) for basename in files if ".pth" in basename]
    return max(paths, key=os.path.getctime)


"""saves the image from the img_arr to the filename"""
def save_image(filename, img_arr):
    plt.imsave(filename, img_arr)


"""
    saves the im1 and im2 concatenated horizontally with the green barrier between them
"""
def concat_images(filename, im1, im2):
    im1_rgb = np.stack([im1, im1, im1], axis=2)
    im2_rgb = np.stack([im2, im2, im2], axis=2)

    border = np.ones([im1.shape[0], 10]) * 255
    border_rgb = np.stack([border * 0, border, border * 0], axis=2)
    res = np.concatenate((im1_rgb, border_rgb, im2_rgb), axis=1)
    res = res / np.amax(res)
    res[res < 0] = 0
    # plt.imshow(res)
    # plt.show()
    plt.imsave(filename, res)


""" 
    counts the normalization factor for the loss function as the number of the background pixels devided by
    the number of the foreground pixels
    the normalization factor is computed across whole dataset

    returns the tensor of size of the picture containing the normalization factor everywhere
"""
def count_norm_factor(trainloader, picture_size):
    background_size = 1
    foreground_size = 1
    i = 0
    for sample in trainloader.dataset:
        i += 1
        img_con, img_label = sample[:2]
        background_size += len(img_label[img_label <= 0.5])
        foreground_size += len(img_label[img_label > 0.5])
        if i > 10:
            break

    pos_weight = torch.ones([picture_size]) * background_size / foreground_size  # All weights are equal to 1
    return pos_weight

"""
saves the checkpoint after the training epoch
checkpoint contains the epoch number, net, and optimizer
param: epoch - epoch number
       net   - neural network to save
       optimizer - current optimizer
"""
def save_checkpoint(epoch, net, optimizer, filename='checkpoint.pth'):
    print("saving checkpoint after epoch: {}".format(epoch), filename)
    checkpoint = {
        'epoch': epoch,
        'net': net,
        'optimizer': optimizer}
    torch.save(checkpoint, filename)

"""
loads the checkpoint from the file
"""
def load_checkpoint(filename='checkpoint.pth'):
    print("loading", filename)
    checkpoint = torch.load(filename)
    epoch = checkpoint['epoch']
    net = checkpoint['net']
    optimizer = checkpoint['optimizer']
    print("loaded checkpoint, epoch {} prepaired".format(epoch + 1), filename)
    return epoch, net, optimizer


""" saves the network net into the filename"""
def save_net(filename, net):
    torch.save(net.state_dict(), filename)


"""loads the network from the filename"""
def load_net(filename):
    # Load saved model parameters
    if torch.cuda.is_available():
        net.load_state_dict(torch.load(filename, map_location=torch.device('cuda:0')))
    else:
        net.load_state_dict(torch.load(filename, map_location=torch.device('cpu')))
    return net

"""Returns a 2D Gaussian kernel array."""
def gkern(kernlen=560, std=140):

    gkern1d = signal.gaussian(kernlen, std=std).reshape(kernlen, 1)
    gkern2d = np.outer(gkern1d, gkern1d)
    return gkern2d

"""
folds the gaussian weighted images (smooth transition between tiles)
"""
def fold_sliced_images(p, N, C, H, W, SLICE_SIZE, STRIDE_SIZE, patches_shape):
    pc = p.cpu()
    kern = gkern(SLICE_SIZE)
    patches = pc.reshape(-1,1, SLICE_SIZE,SLICE_SIZE)
    pp = patches.shape[0]
    folded = np.zeros((H,W))
    folded_weights = np.zeros((H,W))
    sr,sc = 0,0

    for i in range(pp):
        folded[sr:sr+SLICE_SIZE, sc:sc+SLICE_SIZE] = folded[sr:sr+SLICE_SIZE, sc:sc+SLICE_SIZE] + kern*patches[i, 0, :, :].numpy()
        folded_weights[sr:sr+SLICE_SIZE, sc:sc+SLICE_SIZE] = folded_weights[sr:sr+SLICE_SIZE, sc:sc+SLICE_SIZE] + kern
        sc += STRIDE_SIZE
        if sc+SLICE_SIZE > W:
            sc = 0
            sr += STRIDE_SIZE

    folded = folded/folded_weights
    folded = torch.from_numpy(folded)
    folded1 = torch.reshape(folded, (1,1,*folded.shape[:]))  #folded.resize(1,1, *folded.shape[:])
    return folded1

"""
folds the images, the overlaps are averaged
"""
def fold_sliced_images_old(p, N, C, H, W, SLICE_SIZE, STRIDE_SIZE, patches_shape):
    patches = p.reshape(patches_shape)
    patches = patches.reshape(N, C, (H - SLICE_SIZE) // STRIDE_SIZE + 1, (W - SLICE_SIZE) // STRIDE_SIZE + 1,
                              SLICE_SIZE * SLICE_SIZE)
    patches = patches.permute(0, 1, 4, 2, 3)
    patches = patches.squeeze(1)
    patches = patches.view(N, SLICE_SIZE * SLICE_SIZE, -1)
    folded = f.fold(patches, (H, W), kernel_size=SLICE_SIZE, stride=STRIDE_SIZE)
    counter = f.fold(torch.ones_like(patches), (H, W), kernel_size=(SLICE_SIZE, SLICE_SIZE), stride=STRIDE_SIZE)
    folded = folded / counter
    return folded

"""
splits the tiles of the image into multiple minibatches so that the 
neural network does not collapse on the memory limit
"""
def split_batch(x, step):
    splitted = []
    for i in range(0, x.shape[0], step):
        if len(x.shape) == 4:
            a = x[i:i + step, :, :, :]
        elif len(x.shape) == 3:
            a = x[i:i + step, :, :]
        else:
            print("wrong splitting, expected dim 3 or 4, got {}".format(len(x.shape)))
        splitted.append(a)
    return splitted


"""
neural network training

param: num_epochs : number of epochs for training
       net : the neural network
       train_loader : Dataloader with the train set images
       optimizer : neural network optimizer
       criterion : either "DST" or "PIX"
       device : "CPU" or "GPU"
       cur_epoch : number of already performed epochs, if the training is being continued
"""
def train1(num_epochs=2, net=None, train_loader=None, optimizer=None, criterion=None, device="CPU", cur_epoch=0):
    global BATCH_SIZE, MINIBATCH_SIZE
    for epoch in range(cur_epoch, num_epochs):
        running_loss = 0.0
        for i, data in enumerate(train_loader, 0):
            inputs, labels, name = data

            inputs = inputs.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)
            labels = labels.reshape(-1, SLICE_SIZE, SLICE_SIZE)

            split_inputs = split_batch(inputs, MINIBATCH_SIZE)
            split_labels = split_batch(labels, MINIBATCH_SIZE)

            for inputs, labels in zip(split_inputs, split_labels):
                labels = labels.double().to(device)
                optimizer.zero_grad()
                inputs = inputs.to(device)
                inputs = inputs.float()
                outputs = net(inputs)

                C, _, H, W = outputs.shape
                outputs = outputs.view(C, H, W)
                loss = criterion(outputs, labels.float())
                loss.backward()

                optimizer.step()

                running_loss += loss.item()

            # print(np.exp(-loss.item())/(1 + np.exp(-loss.item())))
            if i % BATCH_SIZE == BATCH_SIZE - 1:
                print("[{}, {}], Loss: {}".format(epoch + 1, i + 1, running_loss / BATCH_SIZE))
                # plt.imshow(labels[0])
                # plt.imshow(torch.sigmoid(outputs[0]) > 0.5)
                running_loss = 0.0
        save_checkpoint(epoch + 1, net, optimizer, filename="{}/checkpoint{}.pth".format(resdir,epoch))

"""
neural network testing
param: net : the neural network
       val_loader : Dataloader with the test set images
       device : "CPU" or "GPU"
       batchsize 
       H_original : height of the original image for reconstruction
       W_original : width of the original image for reconstruction
       criterion : "DST" or "PIX"
output:
       saves the network output reconstruction into the destination specified in
       args.dest
       it also saves the concatenated image where the NN output (left) and mask (right)
       are side by side

"""
def test1(net=None, val_loader=None, device=None, batch_size=1, H_original=600, W_original=600, criterion="DST"):
    tot = 0
    num = 0
    for batch in val_loader:
        inputs, labels, name = batch

        inp_shape = inputs.shape

        inputs = inputs.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)
        labels = labels.reshape(-1, 1, SLICE_SIZE, SLICE_SIZE)

        with torch.no_grad():
            split_inputs = split_batch(inputs, MINIBATCH_SIZE)
            outputs = []
            for inputs in split_inputs:
                inputs = inputs.to(device)
                inputs = inputs.float()
                output = net(inputs)
                outputs.append(output)
            mask_pred = torch.cat(outputs, dim=0)
            # print(mask_pred)
            # print(mask_pred.shape)

            # reconstruct original image
            N, C = inp_shape[:2]
            mask_pred = fold_sliced_images(mask_pred, N, C, H_original, W_original, SLICE_SIZE, STRIDE_SIZE, inp_shape)
            labels = fold_sliced_images(labels, N, C, H_original, W_original, SLICE_SIZE, STRIDE_SIZE, inp_shape)
            labels = labels.reshape(-1, *labels.shape[2:])

            if criterion == "DST":
                pred = mask_pred.float()
                pred = (pred - torch.min(pred)) / torch.max(pred)
                labels = labels.float()
                labels = (labels - torch.min(labels)) / torch.max(labels)
            else:
                pred = torch.sigmoid(mask_pred)
                pred = (pred > 0.5).long()
                labels = (labels > 0.5).long()  # necessary due to scaling

            tot += dice_coef.dice_coeff(pred.cpu(), labels.cpu()).item()
            num += 1

            # save images
            for i in range(batch_size):
                if (i >= pred.shape[0]):
                    break

                #to_save_pickle = pred[i, 0, :, :].cpu().numpy()
                #pickle.dump(to_save_pickle, open("{}/{}{}".format(resdir, name[i][:-4], ".p"), "wb"))
                toSave = (pred[i, 0, :, :].cpu() * 255).numpy()
                io.imsave("{}/{}".format(resdir, name[i]), toSave.astype(np.uint8))
                concat_images("{}/{}_combined.png".format(resdir, name[i][:-4]), pred[i, 0, :, :].cpu() * 255,
                              labels[i].cpu() * 255)

    print("dice loss: {}".format(1 - tot / num))



srcdir, resdir = args.src, args.dest #parse_arguments()
if not os.path.isdir(resdir):
    os.system("mkdir {}".format(resdir))

dir_images = os.path.join(srcdir, "contrast")
# dir_images = os.path.join(srcdir, "masks")
image_names = os.path.join(srcdir, "contrast.p")
dir_masks = os.path.join(srcdir, "masks")

# create dataset
names_images = pickle.load(open(image_names, "rb")) if ONLY_NAMES else "local"
if CRIT == "DST":
    transformations = [transf.Rescale((H, W)), transf.DistTransformLabels(), transf.ToTensor(),
                       transf.Slices(SLICE_SIZE, STRIDE_SIZE)]
else:
    transformations = [transf.Rescale((H, W)), transf.ToTensor(), transf.Slices(SLICE_SIZE, STRIDE_SIZE)]
dataset = SegDataset(dir_images, dir_masks, names_images, transforms=transforms.Compose(transformations),
                     only_names=ONLY_NAMES)  #set only names to true if prepared contrast.p
if REBUILD_DATA:
    # split the dataset to the train set and the test set
    n_val = int(len(dataset) * VAL_PERCENT)
    n_train = len(dataset) - n_val
    train, val = random_split(dataset, [n_train, n_val])
    train_loader = DataLoader(train, batch_size=BATCH_SIZE, shuffle=True, num_workers=1)
    val_loader = DataLoader(val, batch_size=BATCH_SIZE, shuffle=False, num_workers=1)

else:  # load prepared dataset
    train_loader = pickle.load(open(os.path.join(resdir,"trainloader.p"), "rb"))
    val_loader = pickle.load(open(os.path.join(resdir, "valloader.p"), "rb"))

pickle.dump(train_loader, open(os.path.join(resdir,"trainloader.p"), "wb"))
pickle.dump(val_loader, open(os.path.join(resdir, "valloader.p"), "wb"))
# compute the normalization factor of loss on background and foreground

pos_weight = count_norm_factor(train_loader, SLICE_SIZE)
pickle.dump(pos_weight, open("{}.p".format("pos_weight.p"), "wb"))


print("data loaded")

if CRIT == "DST":
    criterion = torch.nn.MSELoss()
else:
    criterion = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight.to(device))

net = UNet(n_channels=1, n_classes=1)
net = net.to(device)
lr = 0.01
optimizer = optim.RMSprop(net.parameters(), lr=lr, weight_decay=1e-8, momentum=0.9)

# training
if PRETRAINED == "none":
    # create the net

    print("Starting training")
    train1(num_epochs=EPOCHS, net=net, train_loader=train_loader, optimizer=optimizer, criterion=criterion,
           device=device)
    print("Finished Training")

    if SAVE_NET:
        save_net(os.path.join(resdir,"net_7.pt"), net)
        print("saved net")

elif PRETRAINED == "semi":
    print("Continue training")
    latest_checkpoint_name = latest_file_pth(resdir)  # "checkpoint.pth"
    epoch, net, optimizer = load_checkpoint(latest_checkpoint_name)
    net = net.to(device)

    train1(num_epochs=EPOCHS, net=net, train_loader=train_loader, optimizer=optimizer, criterion=criterion,
           device=device, cur_epoch=epoch)
    print("Finished Training")

    if SAVE_NET:
        save_net(os.path.join(resdir,"net_7.pt"), net)
        print("saved net")

elif PRETRAINED == "full":
    net = load_net(os.path.join(resdir,"net_7.pt"))
    print("Loaded pretrained net")

# testing
net.eval()  # comment out if the minibatchsize is too small
test1(net=net, val_loader=val_loader, device=device, batch_size=BATCH_SIZE, H_original=H, W_original=W, criterion=CRIT)

end = time.time()
print("time: " + str(end - start))



