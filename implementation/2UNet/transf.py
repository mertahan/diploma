"""
    transforms applicable to the image and target from the dataset
"""


from skimage import io, transform
import scipy.ndimage as nd
import torch

"""
Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
"""
class Rescale(object):
    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        img, label = sample[0], sample[1]

        h, w = img.shape[:2]
        new_h, new_w = self.output_size
        new_h, new_w = int(new_h), int(new_w)

        if h == new_h and w == new_w:
            return img, label


        img = transform.resize(img, (new_h, new_w))
        label = transform.resize(label, (new_h, new_w))

        return img, label

"""slices the images into slices of given size with a given stride"""
class Slices(object):
    def __init__(self, size, stride):
        self.size = size
        self.stride = stride

    def __call__(self, sample):
        img, label = sample[0], sample[1]

        img1 = img.unfold(1, self.size, self.stride).unfold(2, self.size, self.stride)
        img = img1

        label1 = label.unfold(0, self.size, self.stride).unfold(1, self.size, self.stride)
        label = label1

        return img, label


"""creates a distance map from the background"""
class DistTransformLabels(object):
    def __init__(self):
        pass

    def __call__(self, sample):
        img, label = sample[0], sample[1]

        label[label>0.5] = 1   # objects
        label[label<=0.5] = 0   # background
        label = nd.distance_transform_edt(label)  # distances from 0, in this case from background

        return img, label



# cuts the upper left hand corner of the given size from the image
class Cut(object):

    def __init__(self, max_h, max_w):
        self.max_h = max_h
        self.max_w = max_w

    def __call__(self, sample):
        img, label = sample[0], sample[1]

        img = img[0:self.max_h, 0:self.max_w]
        label = label[0:self.max_h, 0:self.max_w]

        return img, label

# transforms the image to the tensor
class ToTensor(object):

    def __init__(self):
        pass

    def __call__(self, sample):
        img, label = sample[0], sample[1]

        img = torch.as_tensor(img)
        img = img.view(-1, img.shape[0],img.shape[1])

        label = torch.as_tensor(label)

        return img, label
