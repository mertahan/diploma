
"""
    concats multiple images into one visualization:
	 the contrast images, nn output, mask, original conrast image, original fluorescence image
	 output visualization contains:
		 1. contrast microscopy combined with the nn output
		 2. contrast microscopy combined with the nn output with the couloured false positives, true positives and FNs
		 3. original fluorescence image
		 4. original contrast microscopy image
		 5. fluorescence combined with the contrast microscopy
		 6. contrast microscopy combined with the segmentation obtained from the fluorescence microscopy
    usage:
    python3 visualization_results.py --nnres results_dst_20epochs/resNN20epochs_dst --dest1 visdst

    if the inputNN_all not in ".",
    python3 visualization_results.py --nnres results_dst_20epochs/resNN20epochs_dst --dest1 visdst
                --con contrast.p --flu fluor.p --mask masks

    results:
        combined images in --dest1
"""

import os
import pickle
from skimage import io
import skimage
import numpy as np
from matplotlib import pyplot as plt
from  copy import copy

from skimage.transform import rescale
from skimage import io, transform

local = False

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--con", default="inputNN_all/contrast.p", help="echo the string you use here")
parser.add_argument("--flu", default="inputNN_all/fluor.p", help="echo the string you use here")
parser.add_argument("--mask", default="inputNN_all/masks", help="echo the string you use here")
parser.add_argument("--nnres", default="resultsNN_all/pics_to_vis", help="echo the string you use here")
parser.add_argument("--dest1", default="resultsNN_all/visualization", help="echo the string you use here")
parser.add_argument("--dest2", default="resultsNN_all/visualization2", help="echo the string you use here")
args = parser.parse_args()

if not local:
    contrast_file_names = args.con
    fluoresc_file_names = args.flu
    masksdir = args.mask
    resdir = args.nnres

    res_visual = args.dest1
    res_visual2 = args.dest2
else:
    contrast_file_names = "contrast.p"
    fluoresc_file_names = "fluor.p"
    masksdir = "masks"
    resdir = "results" # "resNN20epochs_pixwise"  # "resNN20epochs_dst" #
    res_visual = "visualization"
    res_visual2 = "visualization2"

if not os.path.isdir(res_visual):
    os.system("mkdir {}".format(res_visual))

"""from aligned names of - con_files = original contrast images,
                            - flu_files = original fluorescence_images
                            - mask files = segmented fluorescence images
           and result files (subset of images, those that were in test set)

        creates the four-tuples  of corresponding picture names
        correspondence between filename in result_files and filename in mask_files is
        the same last name

        :returns array of  (con_fname, flu_fname, mask_fname, res_fname)
"""
def create_quadruples(con_files, flu_files, mask_files, result_files):
    con_files.sort()
    flu_files.sort()
    mask_files.sort()

    res =[]
    for rname in result_files:
        for c, f, m in zip(con_files, flu_files, mask_files):
            if rname.split("/")[-1] == m.split("/")[-1]:
                res.append([c, f, m, rname])
    return res

"""
        takes both fluorescence and contrast microscopy picture and creates the convex combination with given alpha,
        the fluorescence shifts to green, contrast microscopy is grey
            :input
                im_float: float (1 channel) picture
                im_con_float: float (1 channel) picture
                alpha: the coefficient of im_float in the combination
            :returns
                combined (3 channel) picture
"""
def merge_float_ims(im_float, im_con_float, alpha=0.4):
    res = np.stack([im_float , im_float, im_float *0], axis=2)
    res_con = np.stack([im_con_float , im_con_float , im_con_float], axis=2)
    res_con /= np.amax(res_con)
    ult = alpha * res + (1-alpha) * res_con
    return ult / np.amax(ult), np.amax(ult)

"""
:param nn_res: result image from neural network
:param ref: reference image (mask)
:param im_con_float: original contrast microscopy image
:param alpha: coefficient of convex combination
:return: combined image of contrast microscopy and nn_res, with marked FP, FN and TP regions
"""
def merge_and_FP(nn_res, ref, im_con_float, alpha=0.4):

    lay1, lay2, lay3= np.zeros(nn_res.shape), np.zeros(nn_res.shape), np.zeros(nn_res.shape)
    FN = (nn_res == 0) * (ref == 1)
    TP = (nn_res == 1) * (ref == 1)
    FP = (nn_res == 1) * (ref == 0)

    lay1[FN], lay2[FN], lay3[FN] = 0, 1, 1 # 1,0,0 purple 1,0,1
    lay1[TP], lay2[TP], lay3[TP] = 1, 1, 0 #yellow 1, 1, 0
    lay1[FP], lay2[FP], lay3[FP] = 1, 0, 0 #0,1,1cyan


    res = np.stack([lay1 , lay2, lay3], axis=2)
    res_con = np.stack([im_con_float , im_con_float , im_con_float ], axis=2)
    res_con /= np.amax(res_con)
    ult = alpha * res + (1-alpha) * res_con
    return ult / np.max(ult)

"""loads the images from names in the quadruple"""
def images_from_quad(quad):
    new_quad = [1, 1, 1, 1]
    for i, name in enumerate(quad):
        im = io.imread(name)
        new_quad[i] = skimage.img_as_float(im)
        if i != 0:
            new_quad[i] = (new_quad[i] - np.amin(new_quad[i])) / np.amax(new_quad[i])
        else:
            # increase contrast in contrast microscopy image
            quant95 = np.quantile(new_quad[i], 0.97)
            quant05 = np.quantile(new_quad[i], 0.03)
            new_quad[i][new_quad[i] > quant95] = quant95
            new_quad[i][new_quad[i] < quant05] = quant05
            new_quad[i] = (new_quad[i] - np.amin(new_quad[i])) / np.amax(new_quad[i])
    return new_quad

"""
thresholds the image with an input threshold
"""
def binary_transform(im, thresh=0.122):
    im[im > thresh] = 1
    im[im != 1] = 0

"""
concatenates the images in the input fourtuple into 
the output image consisting of six subimages

:param: quad - names of the files
        x,y  - desired size of the resized subimages
        alpha - coefficient of the image combining
"""
def concat_quadruple(quad, x, y, alpha=0.4):

    new_quad = images_from_quad(quad)
    con, fl, mas, res = new_quad

    if local:
        res = transform.resize(res, (x,y))

    binary_transform(res)  # due to dst criteria

    border = np.ones([x, 20])
    border_rgb_vert = np.stack([border * 0, border*0, border ], axis=2)

    combined_with_mask, m = merge_float_ims(mas, con, alpha)
    combined_with_result, _ = merge_float_ims(res, con, alpha)
    combined_with_resultFP = merge_and_FP(res, mas, con, alpha)  #merge_float_ims(res, con)
    combined_originals, _ = merge_float_ims(fl, con, alpha)

    rgb_con = np.stack([con, con, con], axis=2)
    rgb_fl = np.stack([fl, fl, fl], axis=2)

    rgb_mas = np.stack([mas, mas, mas],axis=2)
    rgb_res = np.stack([res,res,res], axis=2)

    del (new_quad)

    row1 = np.concatenate((combined_with_result, border_rgb_vert, combined_with_mask), axis=1)
    row2 = np.concatenate((rgb_con/np.amax(rgb_con)*(1-alpha)/m, border_rgb_vert, rgb_fl), axis=1)
    row3 = np.concatenate((combined_with_resultFP, border_rgb_vert, combined_originals), axis=1)
    #row3_alt = np.concatenate((rgb_res, border_rgb_vert, rgb_mas), axis=1)


    print(row2.shape, row3.shape, row1.shape)

    border_horizontal = np.ones((20,row2.shape[1]))
    border_rgb_hor = np.stack([border_horizontal * 0, border_horizontal*0, border_horizontal ], axis=2)

    two_rows = np.concatenate((row1, border_rgb_hor, row2), axis=0)
    three_rows = np.concatenate((two_rows, border_rgb_hor, row3), axis=0)
    #three_rows_alt = np.concatenate((two_rows, border_rgb_hor, row3_alt), axis=0)
    del(two_rows)
    #two_rows = rescale(two_rows, (0.1,0.1,1), anti_aliasing=False) # todo on grid 0.1, no antialiasing
    three_rows = rescale(three_rows, (0.1,0.1,1), anti_aliasing=False) # todo on grid 0.1, no antialiasing
    #three_rows = rescale(three_rows, 0.1) # todo on grid 0.1, no antialiasing
    #three_rows_alt = rescale(three_rows_alt, scale=(0.05,0.05,1), anti_aliasing=False)

    return three_rows, None #three_rows_alt  #, three_rows_alt


contrast_file_names = pickle.load(open(contrast_file_names, "rb"))
fluoresc_file_names = pickle.load(open(fluoresc_file_names, "rb"))

masks_names = os.listdir(masksdir)
masks_file_names = [os.path.join(masksdir, mname) for mname in masks_names]

res_names = os.listdir(resdir)
res_file_names = [os.path.join(resdir, rname) for rname in res_names]

quads = create_quadruples(contrast_file_names, fluoresc_file_names, masks_file_names, res_file_names)

# if only one image wanted:
#desiredname = "1500cells 24h_C21_T0001F001L01A01Z01C01.png"


i = 0
for quad in quads:
    name = copy(quad[3].split("/")[-1])
    # if name != desiredname:
    #     continue
    three_rows, three_rows_alt = concat_quadruple(quad, 2160,2560)
    io.imsave(os.path.join(res_visual,name), three_rows)

    i+=1







