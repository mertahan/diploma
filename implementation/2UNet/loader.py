"""
    dataloader takes the input images directory and input masks directory,
    and transformation function and returns the loaded data when asked

    if only_names is True, it takes the names_images instead of images directory

    dir_images: directory containing the input images
    names_images: array containing the input images names
    dir_masks: directory containing the masks
    transforms: function to apply to the input image and mask, e.g. resize
    only_names: switches the source of input images from dir_images to names_images

"""

import os
import torch
from skimage import io
import numpy as np
import torchvision
from torch.utils.data import Dataset
from PIL import Image

from matplotlib import pyplot as plt


class SegDataset(object):
    """
        initialization of the dataset, loades the images from
        the directories, applies transforms,
        if only_names is True, it loads contrast images from file names instead
        of the directory
    """
    def __init__(self, dir_images, dir_masks, names_images, transforms, only_names=False):
        self.transforms = transforms
        self.dir_images = dir_images
        self.dir_masks = dir_masks
        self.only_names = only_names
        # load all image files, sorting them to
        # ensure that they are aligned
        if only_names:
            self.imgs = list(sorted(names_images))
        else:
            self.imgs = list(sorted(os.listdir(dir_images)))
        self.masks = list(sorted(os.listdir(dir_masks)))

        # check if the names are aligned properly (in case one file misses everything would be shifted)
        for a,b in zip(self.masks, self.imgs):
            a, b = a.split("/")[-1], b.split("/")[-1]
            a, b = a.split("_"), b.split("_")
            a,b = a[1]+ a[2], b[1] + b[2]

            if a[:12] != b[:12]:
                print("data are probably not aligned: {},{}".format(a,b))

    """yields an item from the dataset"""
    def __getitem__(self, idx):

        if self.only_names:
            img_path = self.imgs[idx]
        else:
            img_path = os.path.join(self.dir_images, self.imgs[idx])

        mask_path = os.path.join(self.dir_masks, self.masks[idx])
        img = io.imread(img_path)
        img = img.astype(np.float32)

        mask = io.imread(mask_path)
        mask = np.array(mask)
        target = torch.as_tensor(mask, dtype=torch.uint8)
        target = target / 255.0 - 0.0001

        if self.transforms is not None:
            img, target = self.transforms([img, target])

        return img, target, self.masks[idx]

    def __len__(self):
        return min(len(self.masks), len(self.imgs))
        #return 10