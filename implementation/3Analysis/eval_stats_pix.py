
"""
    processes the input pickle containing the output from eval_kvantiv_separate2pix.py
    prints the table of sensitivity, precision and Dice loss and constructs histogram of false positives
    usage:
    python3 eval_stats.py --fname PICKLENAME --combined YES
	--PICKLENAME = output pickle from eval_kvantiv_separate2pix.py
	--combined = YES to merge datasets
"""

import pickle

from matplotlib.ticker import AutoMinorLocator

from eval_kvantiv_separate2pix import Data
import numpy as  np
from matplotlib import pyplot as plt
import itertools
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--PICKLENAME", default="stats/stats_dst20epochs.p", help="pickle with stats")
parser.add_argument("--combined", default="YES", help="combine datasets YES/NO")
args = parser.parse_args()


headersg = ["","$med_{24h}$", "$med_{72h}$","$med_{before}$","$med_{only}$",
            "$mean_{24h}$", "$mean_{72h}$", "$mean_{before}$", "$mean_{only}$"]
headersg = ["","$24h_{med}$", "$72h_{med}$","$before_{med}$","$only_{med}$",
         "$24h_{mean}$", "$72h_{mean}$", "$before_{mean}$", "$only_{mean}$"]
row1g = ["sensitivity"] +[-1]*8
row2g = ["precision"] + [-1]*8
row3g = ["Dice loss"] + [-1]*8

ncols = 7
tableg = [
            ["","median sensitivity", "median precision", "mean sensitivity", "stdsen", "mean precision", "stdprec", "median Dice"],
            ["24h"] + [-1]*ncols,
            ["72h"] + [-1]*ncols,
            ["before"]+[-1]*ncols,
            ["only"]+[-1]*ncols
          ]

"""
returns the mean, median and standard deviation of the input array
"""
def mean_med_std(arr):
    return np.round(np.mean(arr),3), np.round(np.median(arr),3), np.round(np.std(arr),3)

"""
prints the input array as the latex table
param: rows - number of rows
       cols - number of columns
       arr - array to be printed
       caption
       label
"""
def print_table_latex(rows, cols, arr, caption="table", label="t1"):
    print("\n")
    print("\\begin{table}[h!]")
    print("\\centering")
    print("\\begin{tabular}{||" + "c "*cols + "||}")
    print("\\hline")
    print( " & ".join([arr[0][i] for i in range(cols)]) + " \\\\ [0.5ex]")
    print("\\hline\\hline")
    for row in range(1, rows):
        print(" & ".join([str(el) for el in arr[row]]) + " \\\\")
    print("[1ex]")
    print("\\hline")
    print("\\end{tabular}")
    print("\\caption{" + caption + "}")
    print("\\label{" + label + "}")
    print("\\end{table}")

"""
prints the input array as the transposed latex table
param: rows - number of rows of original table
       cols - number of columns of original table
       arr - array to be printed
       caption
       label
"""
def print_table_latex_transposed(rows, cols, arr, caption="table", label="t1"):
    print("\n")
    print("\\begin{table}[h!]")
    print("\\centering")
    print("\\begin{tabular}{||" + "c "*rows + "||}")
    print("\\hline")
    print( " & ".join([arr[i][0] for i in range(rows)]) + " \\\\ [0.5ex]")
    print("\\hline\\hline")
    for col in range(1, cols):
        print(" & ".join([str(el) for el in [arr[row][col] for row in range(rows)]]) + " \\\\")
    print("[1ex]")
    print("\\hline")
    print("\\end{tabular}")
    print("\\caption{" + caption + "}")
    print("\\label{" + label + "}")
    print("\\end{table}")

"""
plots the histogram of false positives
"""
def construct_histogram(FPs):
    fig, ax = plt.subplots(1, figsize=(16,6))
    n, bins, patches = plt.hist(FPs, 30, facecolor='green', alpha=0.75)
    xticks = [(bins[idx + 1] + value) / 2 for idx, value in enumerate(bins[:-1])]
    xticks_labels = ["{:.0f}\nto\n{:.0f}".format(value, bins[idx + 1]) for idx, value in enumerate(bins[:-1])]
    plt.xticks(xticks, labels=xticks_labels)
    #plt.xticks(bins, rotation=90)
    plt.show()

"""
saves FP histogram with a given number of bins into the input name file
"""
def hist_2(FPs, name, numbins):
    fig, ax = plt.subplots(1, figsize=(12, 15), facecolor='#1d1135')
    ax.set_facecolor('lavender')
    plt.subplots_adjust(left=0.08, right=0.97, top=0.95, bottom=0.15)

    n, bins, patches = plt.hist(FPs, numbins, facecolor='green',  alpha=0.75)
    minor_locator = AutoMinorLocator(2)
    plt.grid(which='minor', color='#1d1135', lw=0.5)  # ticks

    xticks = [(bins[idx + 1] + value) / 2 for idx, value in enumerate(bins[:-1])]
    xticks_labels = ["{:.0f}\nto\n{:.0f}".format(value, bins[idx + 1]) for idx, value in enumerate(bins[:-1])]
    plt.xticks(xticks, labels=xticks_labels, c='blue', fontsize=25)
    ax.tick_params(axis='x', which='both', length=0)
    plt.yticks([])  # Hide the right and top spines
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    for idx, value in enumerate(n):
        if value > 0:
            plt.text(xticks[idx], value + 0.5, int(value), ha='center', fontsize=25, c='blue')
    plt.xlabel('\nFalse Positives', c='darkgreen', fontsize=35)
    plt.ylabel('Number of Images', c='darkgreen', fontsize=35)
    fig.savefig(name, bbox_inches='tight')

"""
saves FP histogram with a given number of bins into the input name file
"""
def hist_3(FPs, name, numbins):
    #fig, ax = plt.subplots(1, figsize=(28, 15), facecolor='#1d1135')
    fig, ax = plt.subplots(1, figsize=(34, 18), facecolor='#1d1135')
    #ax.set_facecolor('#1d1135')
    ax.set_facecolor('lavender')
    plt.subplots_adjust(left=0.08, right=0.97, top=0.95, bottom=0.15)

    n, bins, patches = plt.hist(FPs, numbins, facecolor='green',  alpha=0.75)
    minor_locator = AutoMinorLocator(2)
    #plt.gca().xaxis.set_minor_locator(minor_locator)
    plt.grid(which='minor', color='#1d1135', lw=0.5)  # ticks

    xticks = [(bins[idx + 1] + value) / 2 for idx, value in enumerate(bins[:-1])]
    #xticks_labels = ["{:.0f}-{:.0f}".format(value, bins[idx + 1]) for idx, value in enumerate(bins[:-1])]
    xticks_labels = ["{:.0f}\nto\n{:.0f}".format(value, bins[idx + 1]) for idx, value in enumerate(bins[:-1])]

    #plt.xticks(xticks, labels=xticks_labels, c='w', fontsize=25)
    plt.xticks(xticks, labels=xticks_labels, c='blue', fontsize=25)
    ax.tick_params(axis='x', which='both', length=0)
    plt.yticks([])  # Hide the right and top spines
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    for idx, value in enumerate(n):
        if value > 0:
            #plt.text(xticks[idx], value + 1, int(value), ha='center', fontsize=25, c='w')
            plt.text(xticks[idx], value + 0.5, int(value), ha='center', fontsize=25, c='blue')
    plt.xlabel('\nFalse Positives', c='darkgreen', fontsize=35)
    plt.ylabel('Number of Images', c='darkgreen', fontsize=35)
    #plt.title(name, fontsize=30, c='#13ca91')
    #plt.show()

    #plt.gcf().subplots_adjust(bottom=0.3)
    #plt.tight_layout()
    fig.savefig(name, bbox_inches='tight')
    #plt.savefig(name, bbox_inches='tight', dpi=fig.dpi)

"""
prints the statistics of the input file
and constructs the latex table
"""
def print_stats(Ps, TPs, FPs, FNs, dices, name, i=0):

    sensitivities = TPs / Ps
    precisions = TPs / (TPs + FPs)
    miss_rate = FNs / Ps
    false_discovery_rate = FPs / (FPs + TPs)
    threat_score = TPs / (TPs + FNs + FPs)

    sens_mean, sens_med, sens_std = mean_med_std(sensitivities)
    prec_mean, prec_med, prec_std = mean_med_std(precisions)
    miss_rate_mean, miss_rate_med, miss_rate_std = mean_med_std(miss_rate)
    FDR_mean, FDR_med, FDR_std = mean_med_std(false_discovery_rate)
    TS_mean, TS_med, TS_std = mean_med_std(threat_score)
    dice_mean, dice_med, dice_std = mean_med_std(dices)

    print("senstivity: ", sens_mean, sens_med, sens_std)
    print("precision: ", prec_mean, prec_med, prec_std)
    print("miss_rate: ", miss_rate_mean, miss_rate_med, miss_rate_std)
    print("FDR: ", FDR_mean, FDR_med, FDR_std)
    print("TS: ", TS_mean, TS_med, TS_std)
    print("dices: ", dice_mean, dice_med, dice_std)

    headers = [" ", "mean", "median", "standard deviation"]
    row1 = ["sensitivity", sens_mean, sens_med, sens_std]
    row2 = ["precision", prec_mean, prec_med, prec_std]
    row3 = ["dice loss", dice_mean, dice_med, dice_std]
    arr = [headers, row1, row2, row3]
    print_table_latex(4, 4, arr, caption=name)
    print_table_latex_transposed(len(arr), len(arr[0]), arr, caption=name)

    row1g[i+1], row1g[i+5] = sens_med, sens_mean
    row2g[i+1], row2g[i+5] = prec_med, prec_mean
    row3g[i+1], row3g[i+5] = dice_med, dice_mean

    a =["", "median sensitivity", "median precision", "mean sensitivity", "stdsen", "mean precision", "stdprec", "median Dice"],

    tableg[i+1][1], tableg[i+1][2], tableg[i+1][3], tableg[i+1][4], tableg[i+1][5], tableg[i+1][6], = \
        sens_med, prec_med, sens_mean, "$\pm$ {}".format(sens_std), prec_mean, "$\pm$ {}".format(prec_std)
    tableg[i + 1][1], tableg[i + 1][2], tableg[i + 1][3], tableg[i + 1][4], tableg[i + 1][5], tableg[i + 1][6], \
    tableg[i + 1][7] = \
        sens_med, prec_med, sens_mean, "$\pm$ {}".format(sens_std), prec_mean, "$\pm$ {}".format(prec_std), dice_med

    print_table_latex(len(tableg), len(tableg[0]), tableg)

def print_pix_stats(Ps, TPs, FPs, FNs, name, i=0):

    sensitivities = TPs / Ps
    precisions = TPs / (TPs + FPs)
    miss_rate = FNs / Ps
    false_discovery_rate = FPs / (FPs + TPs)
    threat_score = TPs / (TPs + FNs + FPs)

    sens_mean, sens_med, sens_std = mean_med_std(sensitivities)
    prec_mean, prec_med, prec_std = mean_med_std(precisions)
    miss_rate_mean, miss_rate_med, miss_rate_std = mean_med_std(miss_rate)
    FDR_mean, FDR_med, FDR_std = mean_med_std(false_discovery_rate)
    TS_mean, TS_med, TS_std = mean_med_std(threat_score)

    print("senstivity: ", sens_mean, sens_med, sens_std)
    print("precision: ", prec_mean, prec_med, prec_std)
    print("miss_rate: ", miss_rate_mean, miss_rate_med, miss_rate_std)
    print("FDR: ", FDR_mean, FDR_med, FDR_std)
    print("TS: ", TS_mean, TS_med, TS_std)

    headers = [" ", "mean", "median", "standard deviation"]
    row1 = ["sensitivity", sens_mean, sens_med, sens_std]
    row2 = ["precision", prec_mean, prec_med, prec_std]
    arr = [headers, row1, row2]
    print_table_latex(len(arr), len(arr[0]), arr, caption=name)

    row1g[i+1], row1g[i+5] = sens_med, sens_mean
    row2g[i+1], row2g[i+5] = prec_med, prec_mean
    row3g[i+1], row3g[i+5] = -1, -1

    tableg[i + 1][1], tableg[i + 1][2], tableg[i + 1][3], tableg[i + 1][4], tableg[i + 1][5], tableg[i + 1][6] = \
        sens_med, prec_med, sens_mean, "$\pm$ {}".format(sens_std), prec_mean, "$\pm$ {}".format(prec_std)

fname = args.PICKLENAME

combined = True if args.combined == "YES" else False
#combined = False

dat = pickle.load(open(fname, "rb"))
if combined:
    Ps = np.array(list(itertools.chain(*dat.Ps)))
    TPs = np.array(list(itertools.chain(*dat.TPs)))
    FPs = np.array(list(itertools.chain(*dat.FPs)))
    FNs = np.array(list(itertools.chain(*dat.FNs)))
    dices = np.array(list(itertools.chain(*dat.dice_losses)))
    names = list(itertools.chain(*dat.names))

else:
    Ps = [np.array(P) for P in dat.Ps]
    TPs = [np.array(TP) for TP in dat.TPs]
    FPs = [np.array(FP) for FP in dat.FPs]
    FNs = [np.array(FN) for FN in dat.FNs]
    dices = [np.array(dice) for dice in dat.dice_losses]
    names = [nms for nms in dat.names]



#construct_histogram(FPs)
cats=["24h_","72h_","before_", "only cells 2_"]

if combined:
    print("\n\n##################")
    print("####  combined ####")
    print("##################" +"\n\n")
    hist_3(FPs, "histcomb", numbins=30)
    print_stats(Ps, TPs, FPs, FNs, dices, "combined")
else:
    for i, name in enumerate(cats):
        print("\n\n##################")
        print("####" + name + "####")
        print("##################\n\n")
        Psi, TPsi, FPsi, FNsi, dicesi = Ps[i], TPs[i], FPs[i], FNs[i], dices[i]
        hist_2(FPsi, "{}".format(name), numbins=10)
        print_stats(Psi, TPsi, FPsi, FNsi, dicesi, name, i)

    headers = ["","$sensitivity_{med}$", "$precision_{med}$", "$Dice loss_{med}$",
               "$sensitivity_{mean}$","$precision_{mean}$","$Dice loss_{mean}$"]
    r1, r2, r3, r4 = ["24h"] + [-1]*6, ["72h"] + [-1]*6, ["before"] + [-1]*6,["only"] + [-1]*6

    arr = [headersg, row1g, row2g, row3g]
    r1[1:4], r1[4:8] = [arr[i][1] for i in range(1,4)], [arr[i][5] for i in range(1,4)]
    r2[1:4], r2[4:8] = [arr[i][2] for i in range(1, 4)], [arr[i][6] for i in range(1, 4)]
    r3[1:4], r3[4:8] = [arr[i][3] for i in range(1, 4)], [arr[i][7] for i in range(1, 4)]
    r4[1:4], r4[4:8] = [arr[i][4] for i in range(1, 4)], [arr[i][8] for i in range(1, 4)]
    arr = [headers, r1, r2, r3, r4]

    print("\n\nfull table")
    print_table_latex(len(arr), len(arr[0]), arr, caption="Dataset comparison", label="tlarge")

    print_table_latex(len(tableg), len(tableg[0]), tableg, caption="newtable", label="none")



