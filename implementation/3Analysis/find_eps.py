"""
finds the 1% quantile minimal distance between cells in the images
usage:
    python find_eps.py --MASKDIR
    MASKDIR contains reference segmentations obtained from the fluorescence images
    from images in MASKDIR prints 1% quantile minimal distance between cells

"""


import pickle
import os
import argparse
from skimage import measure as skm
import numpy as np
from numpy.linalg import norm as euklid
from skimage import io
import cv2

from matplotlib import pyplot as plt

local=False  # (True to use different file names than the program arguments)

parser = argparse.ArgumentParser()
parser.add_argument("--mask", default="inputNN_all/masks", help="echo the string you use here")
parser.add_argument("--nnres", default="resnn_trainset/", help="echo the string you use here")
parser.add_argument("--resfig", default="res_eps", help="echo the string you use here")
args = parser.parse_args()

if not local:
    masksdir = args.mask
    resdir = args.nnres
    resfig = args.resfig
else:
    masksdir = "masks"
    resdir = "resnn_train_dst" #"resnn_trainset" #"pics_to_vis" # "resNN20epochs_pixwise"  #
    resfig=args.resfig
if not os.path.isdir(resfig):
    os.system("mkdir {}".format(resfig))

"""
finds the connected components, computes center of each and eliminates those
smaller than 1/10 of mean size of components
param: picture - input binary image
output: array of tuples (each tuple represents one center of an object),
        epsilon : mean of the nuclei minor axis approximation
"""
def find_centers(picture):
    """
    finds the connected components, computes center of each and eliminates those
    smaller than 1/10 of mean size of components
    """
    labeled_image, num_labels = skm.label(picture, connectivity=2, return_num=True)
    sizes = []
    means = []
    for i in range(num_labels):
        res = np.where(labeled_image == (i + 1))
        sizes.append(res[0].size)


    sizes_np = np.array(sizes)
    mean_size = np.mean(sizes_np)
    for i in range(num_labels):
        if sizes[i] > mean_size/10:
            res = np.where(labeled_image == (i + 1))

            positions = np.array([np.array([x, y]) for x, y in zip(res[0], res[1])])
            mean = np.mean(positions, axis=0)
            means.append(mean)
    return means

"""
mathematical morphology operations:
erosion and dilation are applied to the input image
"""
def closing(im):
    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    min_img = cv2.erode(im, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)
    return dil_img

"""
thresholds the image to the binary image
each value of the image smaller than threshold is zero,
while each greater value is one
param: im  - input image
       thresh - value of the threshold
output: binarized image
"""
def binary_transform(im, thresh=0.122):
    im[im > thresh] = 1
    im[im != 1] = 0

"""
creates the pairs of corresponding image names from the 
NN output directory and the directory with masks
param: result_files - NN output directory name
       mask_files - directory with fluorescence segmentations
output: pairs of matching files
"""
def create_tuples(result_files, mask_files):
    res = []
    for rname in result_files:
        for mask in mask_files:
            if rname.split("/")[-1] == mask.split("/")[-1]:
                res.append([rname, mask])
    return res

def closest_except(el, arr, exc):
    min_dst = np.inf
    clo = None
    idxr = None
    for idx, a in enumerate(arr):
        if idx == exc:
            continue
        dst = euklid(a - el)
        if dst < min_dst:
            min_dst, clo, idxr = dst, a, idx
    return min_dst, clo, idxr

"""
finds the closest objects distance in the input image
"""
def min_dsts(ref):
    binary_transform(ref)
    ref = closing(ref)

    cent_ref = find_centers(ref)

    dsts = []

    for i, c2 in enumerate(cent_ref):
        dst, close, idx = closest_except(c2, cent_ref, i)
        dsts.append(dst)
    return dsts


if __name__=="__main__":
    masks_names = os.listdir(masksdir)
    masks_file_names = sorted([os.path.join(masksdir, mname) for mname in masks_names])
    print(len(masks_file_names))

    res_names = os.listdir(resdir)
    res_file_names = sorted([os.path.join(resdir, rname) for rname in res_names])

    tuples = create_tuples(res_file_names, masks_file_names)

    samplesize = 1000

    dsts = []

    print("tuples {}".format(len(tuples)))
    i = 0
    for im_name, ref_name in tuples:
        print(i, "/", len(tuples), im_name, ref_name)
        i += 1
        ref = io.imread(ref_name)

        ref = ref / np.amax(ref)

        dsts += min_dsts(ref)
        if i > samplesize:
            break

    print(dsts)
    pickle.dump(dsts, open("{}/reseps.p".format(resfig), "wb"))

    dstarr = np.array(dsts)
    eps = np.quantile(dstarr, 0.01)
    print(eps)
