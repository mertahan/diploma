
"""
    usage: find_threshold.py --mask --nnres --resfig
     --mask : directory with reference files
     --nnres : directory with results from nn
     --resfig : directory for saving result plots

     the tuples of compared images are taken from mask and nnres dir,
     for each pair of pictures with same name

     it is important that the pictures are binary so that the components (cells)
     are distinguished correctly

     finds the best threshold for binarization, 
     saves the plots of comparism whenever the currently best threshold changes (saved into resfig)
     prints the best threshold

"""

local = False  # (True to use different file names than the program arguments)

import pickle
import os
import argparse
from skimage import measure as skm
import numpy as np
from numpy.linalg import norm as euklid
from skimage import io
import cv2
import itertools

from matplotlib import pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("--mask", default="inputNN_all/masks", help="echo the string you use here")
parser.add_argument("--nnres", default="resnn_trainset/", help="echo the string you use here")
parser.add_argument("--resfig", default="res_threshold", help="echo the string you use here")
args = parser.parse_args()

class Data():
    def __init__(self, TP, FP, FN, P, dice_losses, thresh, names = [], eps=[]):
        self.TPs = TP
        self.FPs = FP
        self.FNs = FN
        self.Ps = P
        self.dice_losses = dice_losses
        self.names = names
        self.eps = eps
        self.thresh = thresh

restats="none.p"
#restats = "findtdata.p"
#restats = "/home/hanka/PycharmProjects/diplomka/neuronka_eval/findtdata005_023.p"

if not local:
    masksdir = args.mask
    resdir = args.nnres
    resfig = args.resfig
else:
    masksdir = "masks"
    resdir = "resnn_train_dst" #"resnn_trainset" #"pics_to_vis" # "resNN20epochs_pixwise"  #
    #resdir = "/home/hanka/PycharmProjects/diplomka/neuronka/results2/pickles" #"resnn_trainset" #"pics_to_vis" # "resNN20epochs_pixwise"  #
    #resdir = "/media/hanka/500GBHD/new_pick2/new_pickles" #"resnn_trainset" #"pics_to_vis" # "resNN20epochs_pixwise"  #
    resfig=args.resfig

if not os.path.isdir(resfig):
    os.system("mkdir {}".format(resfig))

"""
creates the pairs of corresponding image names from the 
NN output directory and the directory with masks
param: result_files - NN output directory name
       mask_files - directory with fluorescence segmentations
output: pairs of matching files
"""
def create_tuples(result_files, mask_files):
    res = []
    for rname in result_files:
        for mask in mask_files:
            if rname.split("/")[-1] == mask.split("/")[-1]:
                res.append([rname, mask])
            elif rname.split("/")[-1].split(".")[0] == mask.split("/")[-1].split(".")[0]:
                res.append([rname, mask])
    return res

"""
finds the closest (euclidian norm) element to the input element in the input array
param: el : tuple of coordinates
       arr :array of coordinates
output: tuple : coordinates in arr closest to the el
"""
def closest(el, arr):
    min_dst = np.inf
    clo = None
    idxr = None
    for idx, a in enumerate(arr):
        dst = euklid(a - el)
        if dst < min_dst:
            min_dst, clo, idxr = dst, a, idx
    return min_dst, clo, idxr

"""
thresholds the image to the binary image
each value of the image smaller than threshold is zero,
while each greater value is one
param: im  - input image
       thresh - value of the threshold
output: binarized image
"""
def binary_transform(im, thresh=0.5):
    im[im > thresh] = 1
    im[im != 1] = 0

def insdot(lay1,lay2,lay3, d1,d2,d3, dx,dy,dsize):
    r = int(round(dsize/2))
    w,h = lay1.shape
    xmin, xmax = int(max(dx-r, 0)), int(min(dx+r, w))
    ymin, ymax = int(max(dy-r, 0)), int(min(dy+r, h))
    lay1[xmin: xmax , ymin:ymax] = d1
    lay2[xmin: xmax , ymin:ymax] = d2
    lay3[xmin: xmax , ymin:ymax] = d3

"""
creates the visualization of the true positive pixels (green) and objects (green dot),
false positive pixels (black) and objects (red dots),
false negative pixels (yellow) and objects (blue dots)

given the image, reference, and positions of individual FP objects centers,
TP object centers and FN object centers

:param im: output of the NN
       ref: reference image (fluorescence segmentation)
       TPposim : coordinates of the matched objects in the NN image
       TPposref: coordinates of the matched object in the reference image
       FPpos : coordinates of the unmatched objects in the NN image
       FNpos : coordinates of the unmatched objects in the reference image
:output image colored according to overlaps with reference
"""
def plot_comparism4(im, ref, TPposim, FPpos, TPposref, FNpos, name="nonamecomb.png"):

    # br, bg, bb = 204/255,229/255,255/255
    # br, bg, bb = 255/255,229/255,204/255
    # br, bg, bb = 224/255,224/255,224/255
    br, bg, bb = 192/255,192/255,192/255
    lay1, lay2, lay3 = np.ones(im.shape)*br, np.ones(im.shape)*bg, np.ones(im.shape)*bb
    FN = (im == 0) * (ref == 1)
    TP = (im == 1) * (ref == 1)
    FP = (im == 1) * (ref == 0)

    lay1[FN], lay2[FN], lay3[FN] = 1, 1, 0  # 1,0,0 purple 1,0,1
    lay1[TP], lay2[TP], lay3[TP] = 75/255, 1, 75/255  # yellow 1, 1
    lay1[TP], lay2[TP], lay3[TP] = 75/255, 183/255, 75/255  # yellow 1, 1
    lay1[FP], lay2[FP], lay3[FP] = 0, 0, 0  # 0,1,1cyan

    tpcim = [0, 153 / 255, 0/255]
    tpcref = [0, 153 / 255, 0/255]
    #tpcref = [0,103/255,0/255]
    fpc = [1,0,0]
    fnc= [0/255,0/255,204/255]
    fnc= [10/255,10/255,84/255]
    fnc= [20/255,10/255,140/255]
    size = 22
    for x, y in TPposim:
        insdot(lay1, lay2, lay3, *tpcim, x, y, size)
    for x,y in FPpos:
        insdot(lay1, lay2, lay3, *fpc, x, y, size)
    for x,y in TPposref:
        insdot(lay1, lay2, lay3, *tpcref, x, y, size)
    for x,y in FNpos:
        insdot(lay1, lay2, lay3, *fnc, x, y, size)

    res = np.stack([lay1, lay2, lay3], axis=2)
    # plt.imshow(res)
    # plt.show()
    # io.imsave(name, res)
    res = 255*res
    return res.astype(np.uint8)
    #input()

def compare_images(im, ref, thresh, plot=False):
    """

    :param im: binary image segmented by neural network
    :param ref: binary reference image
    """
    binary_transform(im, thresh)

    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    min_img = cv2.erode(im, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)
    im = dil_img

    loss = dice_loss(im, ref)
    if plot:
        cent_im, _ = find_centers(im)
        # cent_ref, max_dst= find_centers(ref)
        cent_ref, _ = find_centers(ref)

        P = len(cent_ref)
        print("found centers", len(cent_im), P)
        TP = 0
        FP = 0
        FN = 0
        TPposim, TPposref, FPpos, FNpos = [], [], [], []

        precomputedf = []
        precomputedg = []
        for c1 in cent_im:
            dst, close, idx = closest(c1, cent_ref)
            precomputedf.append((dst, close, idx))
        for c2 in cent_ref:
            dst, close, idx = closest(c2, cent_im)
            precomputedg.append((dst, close, idx))

        for idx1, el in enumerate(precomputedf):
            dst, _, idx2 = el
            if dst <= max_dst and precomputedg[idx2][2] == idx1:
                TP += 1
                TPposim.append(cent_im[idx1])
            else:
                FP += 1
                FPpos.append(cent_im[idx1])
        for idx1, el in enumerate(precomputedg):
            dst, _, idx2 = el
            if dst <= max_dst and precomputedf[idx2][2] == idx1:
                TPposref.append(cent_ref[idx1])
            else:
                FN += 1
                FNpos.append(cent_ref[idx1])

        # this is meant for 2 distinct images to plot
        # plot_comparism3(im, ref, TPposim, FPpos, TPposref, FNpos)
        implot = plot_comparism4(im, ref, TPposim, FPpos, TPposref, FNpos)
        return implot
    return loss

"""
mathematical morphology operations:
erosion and dilation are applied to the input image
"""
def closing(im):
    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    min_img = cv2.erode(im, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)
    return dil_img

"""
finds the connected components, computes center of each and eliminates those
smaller than 1/10 of mean size of components
param: picture - input binary image
output: array of tuples (each tuple represents one center of an object),
        epsilon : mean of the nuclei minor axis approximation
"""
def find_centers(picture):
    """
    finds the connected components, computes center of each and eliminates those
    smaller than 1/10 of mean size of components
    """
    labeled_image, num_labels = skm.label(picture, connectivity=2, return_num=True)
    sizes = []
    means = []
    for i in range(num_labels):
        res = np.where(labeled_image == (i + 1))
        sizes.append(res[0].size)


    sizes_np = np.array(sizes)
    mean_size = np.mean(sizes_np)
    filtered_sizes = sizes_np[sizes_np> mean_size/10]
    mean_filtered = np.mean(filtered_sizes)
    epsilon = np.sqrt(mean_filtered/np.pi)*2/3
    print("epsilon is: {}".format(epsilon))
    for i in range(num_labels):
        if sizes[i] > mean_size/10:
            res = np.where(labeled_image == (i + 1))

            positions = np.array([np.array([x, y]) for x, y in zip(res[0], res[1])])
            mean = np.mean(positions, axis=0)
            means.append(mean)
    return means, epsilon

"""
computes the dice loss between image and referenece
"""
def dice_loss(im, ref):
    eps = 0.0001
    inter = np.dot(np.reshape(im, (-1,)), np.reshape(ref, (-1,)))
    union = np.sum(im) + np.sum(ref) + eps
    t = (2 * inter + eps) / union
    return 1-t

"""
plots the ys input on y axis and x input on x axis
ys contain lists of y values, each list has a corresponding label 
in the labels
the result is saved into the input file name
param: x : x axis values
       ys : array of arrays, each array has values corresponding to x axis values 
       labels : description of the ys
       name : output plot name
output: plot saved into input file name
"""
def plot_scatter(x, ys, labels, name="noname2"):
    colors = ['blue', 'red', 'green', 'violet']
    plt.figure()
    for i, y in enumerate(ys):
        plt.scatter(x, y, c=colors[i], label=labels[i])
    plt.legend()
    #plt.yticks(np.arange(0, 1, step=0.1))
    plt.xlabel("threshold")
    #plt.xticks(np.arange(len(x)), ( *x ))
    #plt.show()
    plt.savefig(name)



if __name__=="__main__":
    compute = False
    if compute:

        masks_names = os.listdir(masksdir)
        masks_file_names = sorted([os.path.join(masksdir, mname) for mname in masks_names])
        print(len(masks_file_names))

        res_names = os.listdir(resdir)
        res_file_names = sorted([os.path.join(resdir, rname) for rname in res_names])

        tuples = create_tuples(res_file_names, masks_file_names)

        best_loss = np.inf
        best_thresh = -1
        samplesize = 51 #len(tuples)

        print("tuples {}".format(len(tuples)))

        dicelosses, Ps, FPs, FNs, TPs = [], [], [], [], []


        threholds = [0.05, 0.122,0.5]

        for threshold in threholds:
            i = 0
            cum_dloss, cumPs, cumFPs, cumFNs, cumTPs = 0,0,0,0,0
            for im_name, ref_name in tuples:
                print(threshold, i, "/", len(tuples),im_name, ref_name)
                i+=1
                #im = io.imread(im_name)
                im = pickle.load(open(im_name, "rb"))
                ref = io.imread(ref_name)

                #im = im / np.amax(im)
                ref = ref / np.amax(ref)

                dloss = compare_images(im, ref, threshold)
                TP, FP, FN, P, max_dst = 0,0,0,0,18.5

                cum_dloss += dloss
                cumPs += P
                cumFPs += FP
                cumFNs+= FN
                cumTPs += TP

                if i > samplesize:
                    break
            dicelosses.append(cum_dloss/i)
            Ps.append(cumPs)
            FPs.append(cumFPs)
            FNs.append(cumFNs)
            TPs.append(cumTPs)

            implot = compare_images(im, ref, best_loss, plot=True)
            io.imsave("{}/{}_{}".format(resfig, threshold, im_name.split("/")[-1]), implot)

            if cum_dloss/samplesize < best_loss:
                best_loss, best_thresh = cum_dloss/samplesize, threshold
                print("currently best_thresh: {}, best_loss: {}".format(best_thresh, best_loss))
                implot = compare_images(im, ref, best_loss, plot=True)
                io.imsave("{}/{}_{}".format(resfig, threshold, im_name.split("/")[-1]), implot)

        print("best_thresh: {}, best_loss: {}".format(best_thresh, best_loss))

        dat = Data(TPs, FPs, FNs, Ps, dicelosses, threholds)
        pickle.dump(dat, open(restats, "wb"))
    else:

        dat = pickle.load(open(restats, "rb"))

        dat1 = pickle.load(open("findtdata.p","rb"))
        dat2 = pickle.load(open("findtdata005_023.p","rb"))
        threshs = [*dat1.thresh, *dat2.thresh]
        dices = [*dat1.dice_losses, *dat2.dice_losses]
        TPs = np.array([dat1.TPs, dat2.TPs])
        FPs = np.array([dat1.FPs, dat2.FPs])
        FNs = np.array([dat1.FNs, dat2.FNs])
        Ps = np.array([dat1.Ps, dat2.Ps])


        roc = (TPs + FPs)/Ps


        plot_scatter(threshs, [dices, roc], ["Dice loss", "precision/recall"], "t_crit_combined_13.png")
        plot_scatter(threshs, [dices], ["Dice loss"], "t_crit_combined_noROC_13.png")






