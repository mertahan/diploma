
"""
    usage:
    python eval_kvantiv_separate2.py --mask --nnres --dest --resfig
     --mask : directory with reference files
     --nnres : directory with results from nn
     --dest : pickle name to save results
     --resfig : directory with visualization

     the tuples of compared images are taken from mask and nnres dir,
     for each pair of pictures with same name

     it is important that the pictures are binary so that the components (cells)
     are distinguished correctly, or script binarizes

     saves TPs, FPs, FNs, Ps, dice_losses into pickle dest
     TPs, FPs, FNs, Ps are arrays of numbers of size of compared pictures
     TPs, FPs, FNs, Ps are splitted into multiple arrays according to datasets

"""

local = False  # (True to use different file names than the program arguments)

import random
import pickle
import os
import argparse
from skimage import measure as skm
import numpy as np
from numpy.linalg import norm as euklid
from skimage import io
import cv2


from matplotlib import pyplot as plt

"""
data structure holding the statistics of one image:
number of objects/pixels matched/unmatched (FP,FN) with the reference, dice losses,
names
"""
class Data():
    def __init__(self, TP, FP, FN, P, dice_losses, names = [], eps=[], pixTPs=[], pixFPs=[], pixFNs=[], pixPs=[] ):
        self.TPs = TP
        self.FPs = FP
        self.FNs = FN
        self.Ps = P
        self.dice_losses = dice_losses
        self.names = names
        self.eps = eps

        self.pixTPs = pixTPs
        self.pixFPs = pixFPs
        self.pixFNs = pixFNs
        self.pixPs = pixPs

parser = argparse.ArgumentParser()
parser.add_argument("--mask", default="inputNN_all/masks", help="echo the string you use here")
parser.add_argument("--nnres", default="resultsNN_all/pics_to_vis", help="echo the string you use here")
parser.add_argument("--dest", default="resultsNN_all/evalkvantiv/stats_pix_20.p", help="echo the string you use here")
parser.add_argument("--resfig", default="reseval", help="echo the string you use here")
args = parser.parse_args()

if not local:
    masksdir = args.mask
    resdir = args.nnres

    res_stats = args.dest
    resfig = args.resfig
else:
    masksdir = "masks"
    resdir = "resNN20epochs_pixwise"#"resNN20epochs_dst" # #"resNN20epochs_dst" # "resNN20epochs_dst" #"pics_to_vis" # "resNN20epochs_pixwise"  #

    res_stats = "stats/none.p" #stats/statsdst.p"
    resfig="ahoj"#, # "resevaldst_FPs" #"resevaldst"

if not os.path.isdir(resfig):
    os.system("mkdir {}".format(resfig))


def insdot(lay1,lay2,lay3, d1,d2,d3, dx,dy,dsize):
    r = int(round(dsize/2))
    w,h = lay1.shape
    xmin, xmax = int(max(dx-r, 0)), int(min(dx+r, w))
    ymin, ymax = int(max(dy-r, 0)), int(min(dy+r, h))
    lay1[xmin: xmax , ymin:ymax] = d1
    lay2[xmin: xmax , ymin:ymax] = d2
    lay3[xmin: xmax , ymin:ymax] = d3

"""
creates the visualization of the true positive pixels (green) and objects (green dot),
false positive pixels (black) and objects (red dots),
false negative pixels (yellow) and objects (blue dots)

given the image, reference, and positions of individual FP objects centers,
TP object centers and FN object centers

:param im: output of the NN
       ref: reference image (fluorescence segmentation)
       TPposim : coordinates of the matched objects in the NN image
       TPposref: coordinates of the matched object in the reference image
       FPpos : coordinates of the unmatched objects in the NN image
       FNpos : coordinates of the unmatched objects in the reference image
:output image colored according to overlaps with reference
"""
def plot_comparism4(im, ref, TPposim, FPpos, TPposref, FNpos, name="nonamecomb.png"):

    br, bg, bb = 192/255,192/255,192/255
    lay1, lay2, lay3 = np.ones(im.shape)*br, np.ones(im.shape)*bg, np.ones(im.shape)*bb
    FN = (im == 0) * (ref == 1)
    TP = (im == 1) * (ref == 1)
    FP = (im == 1) * (ref == 0)

    lay1[FN], lay2[FN], lay3[FN] = 1, 1, 0  # 1,0,0 purple 1,0,1
    lay1[TP], lay2[TP], lay3[TP] = 75/255, 1, 75/255  # yellow 1, 1
    lay1[TP], lay2[TP], lay3[TP] = 75/255, 183/255, 75/255  # yellow 1, 1
    lay1[FP], lay2[FP], lay3[FP] = 0, 0, 0  # 0,1,1cyan

    tpcim = [0, 153 / 255, 0/255]
    tpcref = [0, 153 / 255, 0/255]
    fpc = [1,0,0]
    fnc= [20/255,10/255,140/255]
    size = 22
    for x, y in TPposim:
        insdot(lay1, lay2, lay3, *tpcim, x, y, size)
    for x,y in FPpos:
        insdot(lay1, lay2, lay3, *fpc, x, y, size)
    for x,y in TPposref:
        insdot(lay1, lay2, lay3, *tpcref, x, y, size)
    for x,y in FNpos:
        insdot(lay1, lay2, lay3, *fnc, x, y, size)

    res = np.stack([lay1, lay2, lay3], axis=2)
    res = 255*res
    return res.astype(np.uint8)

"""
creates the pairs of corresponding image names from the 
NN output directory and the directory with masks
param: result_files - NN output directory name
       mask_files - directory with fluorescence segmentations
output: pairs of matching files
"""
def create_tuples(result_files, mask_files):
    res = []
    for rname in result_files:
        for mask in mask_files:
            if rname.split("/")[-1] == mask.split("/")[-1]:
                res.append([rname, mask])
    return res

"""
splits the image names pairs into four categories according to the dataset
if image contains the name of the category, it is sorted into that category

"""
def split_tuples(tuples, cats=["24h_","72h_","before_", "only cells 2_"]):
    splitted = [[] for _ in range(len(cats))]
    for tuple in tuples:
        _, name = tuple
        for i,cat in enumerate(cats):
            if cat in name:
                splitted[i].append(tuple)
    return splitted

"""
finds the connected components, computes center of each and eliminates those
smaller than 1/10 of mean size of components
param: picture - input binary image
output: array of tuples (each tuple represents one center of an object),
        epsilon : mean of the nuclei minor axis approximation
"""
def find_centers(picture):
    labeled_image, num_labels = skm.label(picture, connectivity=2, return_num=True)
    sizes = []
    means = []
    for i in range(num_labels):
        res = np.where(labeled_image == (i + 1))
        sizes.append(res[0].size)


    sizes_np = np.array(sizes)
    mean_size = np.mean(sizes_np)
    filtered_sizes = sizes_np[sizes_np> mean_size/10]
    mean_filtered = np.mean(filtered_sizes)
    epsilon = np.sqrt(mean_filtered/np.pi)*2/3
    print("epsilon is: {}".format(epsilon))
    for i in range(num_labels):
        if sizes[i] > mean_size/10:
            res = np.where(labeled_image == (i + 1))

            positions = np.array([np.array([x, y]) for x, y in zip(res[0], res[1])])
            mean = np.mean(positions, axis=0)
            means.append(mean)
    return means, epsilon

"""
finds the closest (euclidian norm) element to the input element in the input array
param: el : tuple of coordinates
       arr :array of coordinates
output: tuple : coordinates in arr closest to the el
"""
def closest(el, arr):
    min_dst = np.inf
    clo = None
    idxr = None
    for idx, a in enumerate(arr):
        dst = euklid(a - el)
        if dst < min_dst:
            min_dst, clo, idxr = dst, a, idx
    return min_dst, clo, idxr

"""
thresholds the image to the binary image
each value of the image smaller than threshold is zero,
while each greater value is one
param: im  - input image
       thresh - value of the threshold
output: binarized image
"""
def binary_transform(im, thresh=0.122):
    im[im > thresh] = 1
    im[im != 1] = 0

"""
mathematical morphology operations:
erosion and dilation are applied to the input image
"""
def closing(im):
    kernel1 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    min_img = cv2.erode(im, kernel1)
    dil_img = cv2.dilate(min_img, kernel1)
    return dil_img

"""
compares the input images pixelwise and counts the number of 
FPs, TPs, and FNs
param: im - NN output binarized image
       ref - reference segmentation
output: number of FP pixels, TP pixels and FN pixels
"""
def pix_compare_images(im, ref):
    binary_transform(im)
    im = closing(im)
    ref = closing(ref)

    P = np.sum(ref)
    TP = np.sum(np.round(ref)*np.round(im))
    FP = np.sum(np.round(im)>np.round(ref))
    FN = np.sum(np.round(im)<np.round(ref))
    implot = None

    return TP, FP, FN, P, implot

"""
    :param im: binary image segmented by neural network
    :param ref: binary reference image
    :param max_dst: maximal distance between centres to be considered the same
    :return: TP, FP, FN, P (number of objects in reference)
"""
def compare_images(im, ref, max_dst):

    binary_transform(im)
    im = closing(im)

    ref = closing(ref)

    cent_im, _ = find_centers(im)
    #cent_ref, max_dst= find_centers(ref)
    cent_ref, _ = find_centers(ref)


    P = len(cent_ref)
    print("found centers", len(cent_im), P)
    TP = 0
    FP = 0
    FN = 0
    TPposim, TPposref, FPpos, FNpos =[], [],[],[]

    precomputedf = []
    precomputedg = []
    for c1 in cent_im:
        dst, close, idx = closest(c1, cent_ref)
        precomputedf.append((dst, close, idx))
    for c2 in cent_ref:
        dst, close, idx = closest(c2, cent_im)
        precomputedg.append((dst, close, idx))

    for idx1, el in enumerate(precomputedf):
        dst, _, idx2 = el
        if dst <= max_dst and precomputedg[idx2][2] == idx1:
            TP += 1
            TPposim.append(cent_im[idx1])
        else:
            FP += 1
            FPpos.append(cent_im[idx1])
    for idx1, el in enumerate(precomputedg):
        dst, _, idx2 = el
        if dst <= max_dst and precomputedf[idx2][2] == idx1:
            TPposref.append(cent_ref[idx1])
        else:
            FN += 1
            FNpos.append(cent_ref[idx1])

    implot = plot_comparism4(im, ref, TPposim, FPpos, TPposref, FNpos)

    return TP, FP, FN, P, max_dst, implot


def dot(a,b):
    res = 0
    for i in range(a.size):
        res += a[i] * b[i]
    return res

def dice_loss(im, ref):
    eps = 0.0001
    inter = np.dot(np.reshape(im, (-1,)), np.reshape(ref, (-1,)))
    union = np.sum(im) + np.sum(ref) + eps
    t = (2 * inter + eps) / union
    return 1-t

if __name__=="__main__":
    masks_names = os.listdir(masksdir)
    masks_file_names = sorted([os.path.join(masksdir, mname) for mname in masks_names])

    res_names = os.listdir(resdir)
    res_file_names = sorted([os.path.join(resdir, rname) for rname in res_names])

    tuples = create_tuples(res_file_names, masks_file_names)

    plotname1 = "resNN20epochs_dst/1500cells 24h_C17_T0001F003L01A01Z01C01.png"
    print("tuples", tuples)

    TPs = [[], [], [], []]
    FPs = [[], [], [], []]
    FNs = [[], [], [], []]
    Ps = [[], [], [], []]

    pixTPs = [[], [], [], []]
    pixFPs = [[], [], [], []]
    pixFNs = [[], [], [], []]
    pixPs = [[], [], [], []]

    dice_losses = [[], [], [], []]
    names = [[],[],[],[]]

    maxdsts = []

    splitted_tuples = split_tuples(tuples)
    for j, tuples in enumerate(splitted_tuples):
        i = 0
        for im_name, ref_name in tuples:
            print(i, "/", len(tuples), im_name, ref_name)
            i += 1
            im = io.imread(im_name)
            ref = io.imread(ref_name)

            im = im / np.amax(im)
            print(im.shape)
            ref = ref / np.amax(ref)
            print(ref.shape)

            TP, FP, FN, P, max_dst, implot = compare_images(im, ref, 18.5)
            pixTP, pixFP, pixFN, pixP, piximplot = pix_compare_images(im, ref)
            print("pix", pixP, pixTP, pixFP, pixFN)

            io.imsave("{}/u_{}_{}".format(resfig,FP, im_name.split("/")[-1]), implot)

            TPs[j].append(TP)
            FPs[j].append(FP)
            FNs[j].append(FN)
            Ps[j].append(P)

            pixTPs[j].append(pixTP)
            pixFPs[j].append(pixFP)
            pixFNs[j].append(pixFN)
            pixPs[j].append(pixP)

            names[j].append(ref_name.split("/")[-1])
            maxdsts.append(max_dst)

            print("done sensitivity")
            dice_losses[j].append(dice_loss(im, ref))
            print("done dice")

    d = Data(TPs, FPs, FNs, Ps, dice_losses, names, maxdsts,
             pixTPs=pixTPs,pixFPs=pixFPs, pixFNs=pixFNs, pixPs=pixPs)
    pickle.dump(d, open(res_stats, "wb"))

    dat = pickle.load(open(res_stats, "rb"))


















if __name__=="__main__":
    print("h")
    # c = np.zeros((3,3))
    # c[0,0] =1
    # c[0,2] = 1
    # c[0,1] = 1
    # d = np.zeros((3,3)) + np.eye(3)
    # d[0,1:3] = 1
    # d[1,1] =0
    # print(c)
    # print(d)
    # TP, FP, FN, P = compare_images(c, d, 0)
    # print(TP, FP, FN, P)
    #
    # print(dice_loss(c,d))




    # plt.figure()
    # plt.subplot(211)
    # plt.imshow(im)
    #
    # num = 0
    # for cent in cent_im:
    #     num+=1
    #     plt.plot([cent[1]], [cent[0]], '.', markersize=3, color="red")
    # print(num)
    #
    # plt.subplot(212)
    # plt.imshow(ref)
    #
    # plt.show()

    # plt.plot([p.position.x], [p.position.y], '.', markersize=20)



