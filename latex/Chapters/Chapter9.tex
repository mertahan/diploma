
\chapter{Comparing our Algorithm With the Reference-based Approach}
\label{comparing}

In this chapter, we will analyze the results from our solution presented in Chapter \ref{proposed_sol}.

Firstly, we prepare \textit{the reference-membership table} described in Chapter \ref{standard_app}.
Then, we generate  \textit{the membership-table} with our algorithm discussed in Chapter \ref{proposed_sol}.
We also prepare the list of the \textbf{\textit{Important category I}}.
This list contains clusters, that should represent the well-distinguished genes (see Section \ref{important_cat} for
more details).

Now, we have all the necessary information to compute the error defined in Section \ref{formal_def}.
This error shows, how much our reads assignment differs from the reference-based approach for each subject.
We define the error as 3-dimensional vector $E = (E_1, E_2, E_3)$.
In addition, we define the normalized error $N = (N_1, N_2, N_3)$. 
We will explain each component of the error and provide the corresponding formulas.
For required notation to understand formulas see the formal definition in Section \ref{formal_def}.




\section{Error Explained}
\label{error_explained}

\subsection{The First Component of the Error}
Let us suppose that read A has a vector $v_1$ assigned by our algorithm and vector $v_2$ assigned by STAR.
These vectors should ideally provide the same information.

However, the components of $v_1$ and $v_2$ do not necessarily have the same order. This is caused due to the fact
that though components of $v_1$ express the membership to clusters representing genes, the order of these genes is 
not the same as in $v_2$. Moreover, we do not know which elements in $v_1$
correspond to which elements in $v_2$.

Therefore, we take 2 reads, read \textit{A} and read \textit{B}. For these reads holds that if they belong to the
same gene in the \textit{reference-membership table}, they should be in the same cluster in our table.

To provide a simple example: \\
Suppose we have two vectors in the \textit{reference-table}, A : $A_{ref} = (1,0,0)$ and B : $B_{ref} = (1,0,0)$.
It is clear, that if the dot product of A and B equals 1, both of reads belong to the same gene and only one gene.
On the contrary, if the dot product is 0, they do not share any gene membership. If A and B partially belong to 
multiple genes, the product of components referring to these genes is weighted by the percentage.

All in all, if read vectors in \textit{reference-membership table} and \textit{out-membership table} differ only 
in the order of elements (this order is the same amongst one table), the dot product of two reads with IDs A and B from one table 
should stay the same as the dot product from the other one with the same IDs A and B .

Hence, we sum the squares of differences. Squares will prevent the error from being negative.

If the domain of variables is obvious, we omit it. 
For further purposes the notation read $\in \mathbb{R}$ or read $\in \mathbb M$ means that such a read 
exists in the corresponding membership-table. 

Formally, 

$\text{Sample } S_1 = \{(i,j,k,l) | \, i \neq j, k \neq l,  r_i \in \mathbb{R}, r_j \in \mathbb{R} \}$ 


\begin{equation}
   \label{eq1}
   E_1= \sum_{(i,j,k,l) \in S_1} (v(r_i) \cdot v(r_j) - d_1)^2 , d_1 = 
   \begin{cases}
       v(m_k) \cdot v(m_l)
          & \text{if }
          \!\begin{aligned}[t]
          & \, m_k \in \mathbb{M} \textrm{ and } m_l \in \mathbb{M}, \\
          & \, ID(m_k) = ID(r_i), \\
          & \, ID(m_l) = ID(r_j) \\
          \end{aligned}
   \\
   0  & \text{otherwise }
   \end{cases}
\end{equation}

The variable $d_1$ in \ref{eq1} represents the dot product in the \textit{out membership table}.
Hence, we can only compute it if the reads with corresponding IDs appear in the 
\textit{out membership table}. If we removed the reads during our solution (in 
the table creation with threshold or during DBSCAN sampling),  the $E_1$ increases
(as we subtract $0$ from the positive number instead of another positive number).



Consequently, we normalize $E_1$ to obtain $N_1$ by the number of summed elements (couples of compared reads).
Moreover, as we powered the difference that is between $-1$ and $1$, we decreased the error. 
Therefore, we will take the square root of the normalized error. Hence, we obtain the quadratic mean $$N_1 = \sqrt{\frac{E_1}{|S_1|}} {.}$$



\subsection{The Second Component of the Error}

The second component is highly similar to the first one. Except, we focus on reads we consider well-distinguished by
our method. These reads are defined by having non-zero elements on the position corresponding to any cluster
from \textit{the important cathegory} (described in Section \ref{important_cat}).

This enables us to avoid computing error on uniformly expressed genes and noise.
However, as the goal is to minimize the error, this could result to not marking any of the clusters as important.
Therefore, the third component is necessary.

Formally, 

\begin{equation*}
   \textrm{Sample } S_2 = \{(i,j,k,l) |
   \!\begin{aligned}[t]
      & \, \, i \neq j, k \neq l, \\
      & \, m_i \in \mathbb{M}, m_j \in \mathbb{M}, \\
      & \, \exists o  \in \mathbb{I}: \, v(m_i)_o \neq 0,  \\
      & \, \exists p \in \mathbb{I}: \, v(m_j)_p \neq 0 \} 
   \end{aligned} 
\end{equation*}



\begin{equation}
   \label{eq2}
   E_2= \sum_{(i,j,k,l) \in S_2} (v(m_i) \cdot v(m_j) - d_2)^2 , d_2 = 
   \begin{cases}
       v(r_k) \cdot v(r_l)
          & \text{if }
          \!\begin{aligned}[t]
          & \, r_i \in \mathbb{R} \textrm{ and } r_j \in \mathbb{R}, \\
          & \, ID(m_i) = ID(r_k), \\
          & \, ID(m_j) = ID(r_l) \\
          \end{aligned}
   \\
   0  & \text{otherwise }
   \end{cases}
\end{equation}

The variable $d_2$ in \eqref{eq2} works the same as $d_1$ in the first element of
the error. Except, this time we pick the reads from 
the \textit{out membership table}
and we are searching for the same IDs in the \textit{reference membership table}. 
As the \textit{reference membership table} is composed only of reads that were aligned to 
the reference, it is possible that some of the reads exist only in the \textit{out 
membership table}.

Similarly to the normalization of $E_1$, we normalize $E_2$ as follows:
$$N_2 = \sqrt{\frac{E_2}{|S_2|}}  {.} $$


\begin{subsection}{The Third Component of the Error}
   Imagine that read \textit{A} is marked important. All reads belonging to the same gene should also be marked 
   important.
   
   The third component of error expresses the number of reads that should be marked important, but are not.
   In this step, for each important read \textbf{A} from \textit{reference-membership table} we find all reads belonging to the same gene and not marked important.\\
   To obtain error caused by omitting some important cluster, we firstly sum membership to clusters of reads marked unimportant. 
   As a result, we have the partial error.
   After that, we normalize the partial error by the number of summed elements, obtaining the normalized partial error. 
   We weigh these partial errors by the percentage of membership of the particular read. 
   After that, we sum the obtained results of all reads marked important, gaining the infromation about the rate of the reads marked unimportant despite being 
   important.
   Finally, we multiply this rate by number of unimportant reads.\\

   
   Formally, 
   we will denote significant reads from the \textit{reference table} as SR and non significant reads as NS. \\

   $$\textrm{SR} = \{ r | \, \exists m \exists k : v(m)_k \neq 0, ID(m) = ID(r), m \in \mathbb{M} \}$$
   $$\textrm{NS} =  \{r | \, r \in \mathbb{R}, r \not \in \textrm{SR}   \} $$
   $$\textrm{avgnonS} = \frac{1}{|\textrm{NS}|} \sum_{r \in \textrm{NS}} v(r)$$

   \begin{equation}
      \label{eq3}
      E_3= |\textrm{NS}|  \sum_{r \in \textrm{SR}} v(r) \cdot \textrm{avgnonS} 
   \end{equation}

   To avoid a long table search, we implement it more efficiently, though equivalent. See Chapter \ref{implementation} for more details.

   Lastly, to get the normalized error we perform a normalization process similarly to previous errors.
   $$N_3  = \frac{E_3}{|\textrm{SR}|}$$ for $E_3$ from \eqref{eq3}.




\end{subsection}

\section{Sampling}

However, it would be very computationally expensive, if we computed dot products for every pair of reads.
As there are roughly $4000000$ reads in \textit{the reference-membership table}, it would result in 
$\binom{4000000}{2}$ pairs. That is roughly $7 \cdot 10^{12}$ pairs.

Therefore, we will use sampling and take only 0.0009\% of pairs (thus normalizing error by the sample size, which
is around $250000$).

