\chapter{Evaluation of the Drug Effects}
\label{svm}

Images in all datasets can be split into multiple categories according to the biological treatment of the cells.
These treatments include Topotecan, Daunorubicin, Etoposide, DMSO,
and no treatment. We assume that the treatment may influence some properties of the cells, which can be calculated from the image, 
such as the density, size, or shape of the cell nuclei.

In this section, we aim to train a classifier that predicts a treatment given a nuclei segmentation.
First, we create a classifier on the reference image segmentation (originating from the fluorescence modality).

Then, we compare the performance of the classifier on the reference images with the performance on the neural network output segmentation.

\section{Preprocessing}
\label{preprocsec}
In this section, % we will introduce the segmentation image preprocessing.

We preprocess the segmentations so that each image consists only of connected components representing the nuclei.

%Our aim is that each image consists only of connected components representing the nuclei.
Moreover, the nuclei should not touch each other so that we correctly determine their number and size.

The first preprocessing step is noise elimination. 
That is, we apply morphology closing operation.
Furthermore, we eliminate components smaller than one-tenth of the mean nuclei size in the particular image.

We already observed touching nuclei in some of the image segmentation.
Thus, the first step before the feature extraction is to split the individual nuclei.
There are multiple methods  (e.g. \cite{separation}, \cite{autowatershed}) available. 
We applied the watershed method from the OpenCV library \cite{opencv_library}.
Generally, the watershed method performs well.

Nonetheless, in several cases, it splits one nucleus into two or does not split two touching nuclei.
Figure \ref{watershedsplit} illustrates such phenomenon.

% \begin{figure}
%     \centering
%     \includegraphics[width=8cm]{Figures/abcde.png}
%     \caption[Watershed borders]{
%         The figure shows a cut from the image with the red nuclei borders found by the watershed.
%         We can see that the watershed nicely split the two nuclei in the bottom right-hand corner, while the two merged cells in the top left-hand corner remain merged.
%     }
%     \label{watershedsplit}
% \end{figure}

\begin{figure}
    \centering
    \subfloat[Original segmentation (zoomed)]{{\includegraphics[width=6.7cm]{Figures/abcde_original.png}\label{orig_wat} }}
    \hskip 1ex
    \subfloat[Watershed preprocessing (zoomed)]{{\includegraphics[width=6.7cm]{Figures/abcde.png}\label{wat1}}}
    
    \subfloat[Original segmentation (zoomed)]{{\includegraphics[width=6.7cm]{Figures/svmfig/original1500cells before_D14_T0001F002L01A01Z01C01.png}\label{orig_wat} }}
    \hskip 1ex
    \subfloat[Watershed preprocessing (zoomed)]{{\includegraphics[width=6.7cm]{Figures/svmfig/colored1500cells before_D14_T0001F002L01A01Z01C01.png}\label{wat}}}
    
    \subfloat[Original segmentation (zoomed)]{{\includegraphics[width=6.7cm]{Figures/svmfig/originalonly cells 2_C09_T0001F003L01A01Z01C01.png}\label{orig_wat} }}
    \hskip 1ex
    \subfloat[Watershed preprocessing (zoomed)]{{\includegraphics[width=6.7cm]{Figures/svmfig/coloredonly cells 2_C09_T0001F003L01A01Z01C01.png}\label{wat}}}
    
    
    
    \caption[Classification accuracy when changing regularization constant]{
        The figure shows examples of the images with each separated nucleus colored with a different color.

        In zoomed Subfigure \ref{wat1} we can see that the watershed nicely split the two nuclei in the bottom right-hand corner, while the two pink merged cells in the top left-hand corner remain merged.
        Generally, some of the nuclei are split, and some remain attached.
    }
    \label{watershedsplit}
  \end{figure}




As the touching nuclei are not very common, it might be more damaging to accidentally split one nucleus into more than not performing
the watershed at all.
Thus, we will evaluate both possibilities and see which leads to the better classification.

In the next section, we will discuss the image feature extraction.

\section{Feature Extraction}
\label{featuresec}

In order to train the classifier, we first need to extract the features from individual images.
In this section, we present the selected features.
Let us have a connected component C corresponding to a nucleus, which is a set of the (x,y) coordinates belonging to the component (we use the 8-connectivity on the grid).
Each image contains multiple such components (nuclei).

We define the component fetures as follows:
\begin{enumerate}
    \item \label{area} $A_{C}$ (area of nucleus) represents the size of the component, $A_{C} = |C|$.
    \item $G_{C}$ (granularity of nucleus) (\cite{separation}) \begin{itemize}
        \item Let us define a perimeter $P_C $ as the number of pixels at the edge of the component.
        %\item $P_C  = |\{(x,y) \in C : \\ |\{(x_n,y_n) \in C : (x_n, y_n) \neq (x,y),  |x-x_n| \leq 1 \wedge |y-y_n| \leq 1 \} |  < 8     \}|$
        \item $P_C  = |\{(x,y) \in C : \\ \exists (x_n,y_n) \notin C : |x-x_n| \leq 1 \wedge |y-y_n| \leq 1 \}|$
        \item In other words, $P_C$ is the number of such pixels that there exists a pixel in the 8-neighborhood not belonging to the component, | | denotes cardnality.
        %\item In other words, $P_C $ is the number of such pixels of the component that have less than 8 neighboring pixels in the component (| | denotes cardinality).
        \item Then the granularity of the component is the ratio between the component perimeter and the area.
        \item $G_{C} = P_C /A_{C}$
    \end{itemize}  
    \item Hu-invariants $L{23}$, $L_{24}$ (\cite{goshtasby2012image}, \cite{separation}) are based on central moments of inertia. Žunić (\cite{huinvariants}) says L23 can be seen 
          as a circularity measure, because the minimal value ($1/2\pi$) is obtained for a circular shape.
          $L_{23}$ and $L_{24}$ are defined as follows:
          \begin{itemize}
              \item $L_{23} = \eta_{20} + \eta_{02}$
              \item $L_{24} = (\eta_{20} + \eta_{02})^2 + 4 \eta_{11}^2$
              \item $\eta_{pq} = \frac{\mu_{pq}}{(\mu_{00})^{\frac{p+q}{2} + 1}}$
              \item $\mu_{pq} = \sum_{(x,y) \in C} (x-x_c)^p (y-y_c)^p  $ %f(x,y)
              \item $(x_c, y_c)$ equals the center of mass of the component. \\ $(x_c,y_c) = (\frac{1}{|C|} \sum_x x,  \frac{1}{|C|} \sum_y y)$
              %\item In our case the $f(x,y) = 1$ for all coordinates belonging to the component. 
          \end{itemize}
    
    \item Major and minor axes lenghts, $C_{maj}$, $C_{min}$ (as the components resemble ellipses).
          We compute them as the two times square roots of the coordinates covariance matrix (D) eigenvalues. \\
          $D = \frac{1}{|C|} \sum_{(x,y) \in C} [x-x_c,y-y_c][x-x_c,y-y_c]^T$, where $[x,y][x,y]^T$ denotes outer product. \\
          Let $\lambda_1 \geq \lambda_2$ be the eigenvalues of the $D$. \\
          $C_{maj} = 2 \sqrt{\lambda_1}$, $C_{min} = 2 \sqrt{\lambda_2}$.
    \item \label{ratio} $R_{aspect} = C_{maj} / C_{min}$ denotes the aspect ratio.
    

\end{enumerate}

We aggregate the extracted features of individual components through each image.
Namely, the final features are means, medians, and standard deviations of features \ref{area}-\ref{ratio}.

Moreover, we aggregate some of the categories through the normalized histogram with a specified range
(306 - 11924 for $A_C$, 50.0 - 784 for $P_C$, 1.0713 - 37.9643 for aspect ratio) and bin number of value ten.
The features aggregated through histograms are $A_{C}$, $P_C $ and $R_{aspect}$.

The last feature is the total nuclei area in the image. That is the sum of individual nuclei areas.
The result of the feature extraction is $52$ element feature vector for each image.


\section{Classifier Training}

We decided to train an SVM classifier to predict the nuclei treatment.
Each image has one of five possible treatments.

However, the standard SVM is meant for two categories. 
We used the SVM implementation from the sklearn library with an RBF kernel.

% trainset 352
% valset 234
First, we created a set of feature vectors F. That is, we applied the processing discussed in Section \ref{preprocsec} and then the feature 
extraction as described in Section \ref{featuresec} on the reference-segmentation images from the fluorescence modality.
Then, we establish a set of feature vectors N obtained from the neural network output images (251 elements).
These two sets (F and N) originate from mutually exclusive images (different microscopy samples).
After that, we split the set F into the training set ($60\%$ of the set F: 352 elements) and the validation set (234 elements).

We then trained the classifier with different regularization constants and compared the performance between training set,
 validation set, and the performance on the neural network output segmentation.
 
 To fine-tune the parameters on
 a pairwise classification between Topotecan and Etoposide (thus, we eliminate the other elements from the training, validation, and test set).
 

 We define accuracy as the number of correctly determined images divided by the number of 
 the classified images.
 
As the feature vectors are quite long, we first perform a feature selection. 
That means we choose only a subset of features that give us the most information.
The selection is performed via sklearn method SelectKBest based on Chi2 test between the features.
Figure \ref{featuresim} shows the influence of the number of selected features on the accuracy.
We observe two local maxima at a value of 10 and a value of 35 features. For the next experiments, we select 35 features.


\begin{figure}
    \centering
    \includegraphics[width=9.5cm]{Figures/svmfig/featurepairwise_compared_split.png}
    \caption[Feature selection]{
        The figure shows the impact of the number of selected features on the accuracy.
    }
    \label{featuresim}
\end{figure}

Figure \ref{Cim} shows that the best regularization constant equals $0.25$.
The accuracy on the validation set is $69\%$, while on the neural network output, it reaches $67\%$.
%The accuracy on the validation set is $40\%$, while on the neural network output, it reaches $39\%$.

\begin{figure}
    \centering
    \includegraphics[width=9.5cm]{Figures/svmfig/Ccompared_05pairwise_nosplit.png}
    \caption[Regularization impact on SVM]{
        The figure shows the impact of the regularization constant on the accuracy.
    }
    \label{Cim}
\end{figure}

We also tried polynomial kernels with different parameter settings, but the accuracy did not improve.
Figure \ref{coef0im} shows the impact of the polynomial kernel coefficient when the regularization constant is $0.25$ (the best performing).
The best validation accuracy with the polynomial kernel reaches a slightly higher value than the RBF kernel :$75\%$, % 92.04771371769384 38.24701195219124.
while the corresponding neural-network output classification reaches $70\%$ accuracy. Thus, the polynomial kernel classification reaches slightly better results
than the RBF kernel.

\begin{figure}
    \centering
    %\includegraphics[width=9.5cm]{Figures/svmfig/c0pairwise_compared_nosplit.png}
    \includegraphics[width=9.5cm]{Figures/svmfig/c0compared05_nosplit.png}
    \caption[Polynomial kernel]{
        The figure shows the performance with the polynomial kernel with different coef0.
    }
    \label{coef0im}
\end{figure}


Consequently, we compare the results obtained with and without the preprocessing discussed in Section \ref{preprocsec}.
We observe a similar performance of the classifier without the preprocessing and the classifier without the preprocessing.
With the preprocessing, we reached the accuracy value $73\%$ on the training set, $65\%$ on the validation set,
and $66\%$ accuracy on the neural network output segmentation.

With the preprocessing, we reached the accuracy value $70\%$ on the training set, $65\%$ on the validation set,
and $62\%$ accuracy on the neural network output segmentation.


In Table \ref{svmpairwise}, we show the pairwise classification performance also for other pairs of treatment.

 \begin{table}[ht!]
    \centering

    \begin{tabular}{|| l l  l |   l| l| l ||}
        \hline
        \multicolumn{3}{|l|}{Pairwise comparison} & 
        % \parbox[t]{4cm}{ pairwise comparison } & 
        % \parbox[t]{0.3cm}{  } & 
        % \parbox[t]{1.3cm}{ } & 
        \parbox[t]{2cm}{training \\ accuracy } & 
        \parbox[t]{2cm}{validation \\ accuracy} &
        \parbox[t]{2cm}{nn output \\ accuracy}  \\[0.5ex] \cline{1-3}

    \hline\hline
    Topotecan & vs & Daunorubicin & 0.72 & 0.51 & 0.47 \\
    Topotecan & vs & Etoposide & 0.73 & 0.65 & 0.66 \\
    Topotecan & vs & DMSO & 0.81 & 0.73 & 0.67 \\
    Topotecan & vs & no treatment & 0.85 & 0.78 & 0.73 \\
    Daunorubicin & vs & Etoposide & 0.71 & 0.69 & 0.67 \\
    Daunorubicin & vs & DMSO & 0.76 & 0.79 & 0.65 \\
    Daunorubicin & vs & no treatment & 0.81 & 0.84 & 0.75 \\
    Etoposide & vs & DMSO & 0.79 & 0.77 & 0.63 \\
    Etoposide & vs & no treatment & 0.78 & 0.8 & 0.65 \\
    DMSO & vs & no treatment & 0.76 & 0.57 & 0.54 \\
    [1ex]
    \hline
    \end{tabular}
    \caption[Pairwise classification]{
        The table illustrates our ability to distinguish between different cell treatments pairwise. 
        We obtain the training and validation dataset by splitting the reference segmentations set. After the model training, 
        we compare the classifier's performance on both the validation dataset and the output of the neural network.
        Results presented above emerge from the experiment without the watershed preprocessing.
    }
    \label{svmpairwise}
\end{table}
    


We can see that neither two categories are well distinguished. Although the accuracy is mostly above $50\%$, it does not 
exceed $75\%$.
Hence, we assume that the pairs with around $50\%$ accuracy cannot be well-distinguished given the extracted features.
We suspect that the internal structure of nuclei has to be taken into account.
Thus, the detailed segmentation of the nuclei components might improve the results in the future.
Also, the successfulness of the classification on the fluorescence-based segmentation is comparable to the results on the neural network output segmentation.


As for the multiclass classification, we used the one-versus-rest generalization. That means 
the ultimate classifier consists of multiple one-versus-rest classifiers and therefore is capable of predicting more 
categories.

With the multiclass predictor, we obtained the validation accuracy $45\%$, and nn output accuracy $35\%$ .

Figure \ref{occurclasses} shows the distribution over the treatment categories.
The dummy classifier predicting the most numerous category has an accuracy of $29\%$ .

\begin{figure}
    \centering
    \includegraphics[width=9.5cm]{Figures/bar_occurences.png}
    \caption[Classes occurences]{
        The figure shows the distribution over the classes in the training dataset.
    }
    \label{occurclasses}
\end{figure}

Thus, the SVM overperforms the dummy predictor. However, the classifier is not able to precisely predict the categories.
It might be necessary to also study the inner components of nuclei, not only the features we extracted. However, that would need more detailed segmentation. \\ \\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%














% Figure \ref{coef0fig} shows the impact of the polynomial kernel coefficient when the regularization constant is one (the best performing).
% The best validation accuracy with the polynomial kernel reaches the same value as the RBF kernel :$41\%$, % 92.04771371769384 38.24701195219124.
% while the corresponding neural-network output classification reaches $38\%$ accuracy; thus, the polynomial kernel did not improve the classification.

%%watershed preprocessing
%  We also compare the results with and without watershed nuclei splitting preprocessing. Figure \ref{Csfig} shows the results.
%  We can see that no nuclei splitting leads to more similar performance on the validation set and the neural network 
%  output segmentation.

% \begin{figure}
%     \centering
%     \subfloat[Watershed preprocessing]{{\includegraphics[width=7.4cm]{Figures/Ccompared_split.png}\label{split_Cs} }}
%     \hskip -4ex
%     \subfloat[No preprocessing]{{\includegraphics[width=7.4cm]{Figures/Ccompared_nosplit.png}\label{nosplit_Cs}}}
  
    
%     \caption[Classification accuracy when changing regularization constant]{
%         The figure illustrates how the training and validation accuracy develop with different regularization constant C.
%         We also show the accuracy of the classifier on the output of the pix2pix neural network.
%         Subfigures compare the results of the training with the watershed preprocessing with the results from training without the preprocessing.
%     }
%     \label{Csfig}
%   \end{figure}
% %watershed preproces



 


% \begin{figure}
%     \centering
%     \subfloat[Watershed preprocessing]{{\includegraphics[width=7.4cm]{Figures/c0compared_split.png}\label{split_coef0} }}
%     \hskip -4ex
%     \subfloat[No preprocessing]{{\includegraphics[width=7.4cm]{Figures/c0compared_nosplit.png}\label{nosplit_coef0}}}
  
    
%     \caption[Classification accuracy when changing regularization constant]{
%         The figure illustrates how the training and validation accuracy develop with different coef0 constant 
%         in the polynomial kernel.
%         We also show the accuracy of the classifier on the output of the pix2pix neural network.
%         Subfigures compare the results of the training with the watershed preprocessing with the results from training without the preprocessing.
%     }
%     \label{coef0fig}
%   \end{figure}




% Thus, the SVM overperforms the dummy predictor. However, our classifier is far from perfect.
% Therefore, we inspect the pairwise classification successfulness. 
% We present the result in Table \ref{svmpairwise} .

% \begin{table}[ht!]
%     \centering

%     \begin{tabular}{|| l l  l |   l| l| l ||}
%         \hline
%         \multicolumn{3}{|l|}{Pairwise comparison} & 
%         % \parbox[t]{4cm}{ pairwise comparison } & 
%         % \parbox[t]{0.3cm}{  } & 
%         % \parbox[t]{1.3cm}{ } & 
%         \parbox[t]{2cm}{training \\ accuracy } & 
%         \parbox[t]{2cm}{validation \\ accuracy} &
%         \parbox[t]{2cm}{nn output \\ accuracy}  \\[0.5ex] \cline{1-3}

%     \hline\hline
%     Topotecan & vs & Daunorubicin & 0.78 & 0.58 & 0.47 \\
%     Topotecan & vs  & Etoposide & 0.72 & 0.69 & 0.69 \\
%     Topotecan & vs & dmso & 0.81 & 0.74 & 0.65 \\
%     Topotecan & vs & no treatment & 0.83 & 0.78 & 0.7 \\
%     Daunorubicin & vs  & Etoposide & 0.74 & 0.63 & 0.64 \\
%     Daunorubicin & vs & dmso & 0.77 & 0.74 & 0.61 \\
%     Daunorubicin & vs & no treatment & 0.84 & 0.76 & 0.69 \\
%     Etoposide & vs & dmso & 0.77 & 0.69 & 0.58 \\
%     Etoposide & vs & no treatment & 0.82 & 0.76 & 0.63 \\
%     dmso & vs & no treatment & 0.68 & 0.56 & 0.55 \\
%     [1ex]
%     \hline
%     \end{tabular}
%     \caption[Pairwise classification]{
%         The table illustrates our ability to distinguish between different cell treatments pairwise. 
%         We obtain the training and validation dataset by splitting the reference segmentations set. After the model training, 
%         we compare the classifier's performance on both the validation dataset and the output of the neural network.
%         Results presented above emerge from the experiment without the watershed preprocessing.
%     }
%     \label{svmpairwise}
% \end{table}
    


% We can see that neither two categories are well distinguished. Although the accuracy is mostly above $50\%$, it does not 
% exceed $70\%$.
% Hence, we assume that the pairs with around $50\%$ accuracy cannot be well-distinguished given the extracted features.
% We suspect that the internal structure of nuclei has to be taken into account.
% Thus, the detailed segmentation of the nuclei components might improve the results in the future.



TODO





