
% \chapter{Introduction to the Clustering Methods} %

% \label{Chapter4} 

\section{Classifier Loss Selection}
\label{unetlossec}

For the classifier to learn, it is rather important to choose the proper loss function.
So far, we have tried two approaches.

\subsection{Pixel-wise Classifying}
This section will describe the classifier that assigns each pixel the probability of the pixel being the foreground (nuclei).
This classifier tries to predict the same output as the reference mask. That means the value $1$ for pixel containing nuclei
and $0$ for the background pixel.

To achieve this, we used binary cross-entropy as the loss function. Additionally, we improved the performance of the neural network
by weighting the components of cross-entropy loss by the occurrence ratio of the foreground and background.
This alternation significantly enhanced the result classification.
The resulting loss is, therefore, the mean of weighted binary cross-entropy losses of each pixel.

Formally, we define the loss as follows:
\begin{itemize}
    \item $I$: \begin{itemize}
        \item[$\cdot$] $I$ represents the input image, $I_{(x,y)}$ is a pixel value on the position $(x,y)$
        \item[$\cdot$] $|I|$ represents the number of pixels
    \end{itemize}
    \item $\hat{p}_{F(x,y)}$: \begin{itemize}
        \item[$\cdot$] predicted value of the pixel with coordinates $(x,y)$
        \item[$\cdot$] can be interpreted as the probability that the cell is the foreground 
    \end{itemize}
    \item $\hat{p}_{B(x,y)}$: \begin{itemize}
        \item[$\cdot$] analogically to the $\hat{p}_{F(x,y)}$,  $\hat{p}_{B(x,y)}$ stands for the probability, that the cell is 
        background 
        \item[$\cdot$]$\hat{p}_{B(x,y)} = 1 - \hat{p}_{F(x,y)}$
    \end{itemize}
    \item $p_{F(x,y)}$: \begin{itemize}
        \item[$\cdot$] reference value of the pixel with coordinates $(x,y)$
        \item[$\cdot$] can be interpreted as the ground truth probability that the cell is the foreground
        \item[$\cdot$] $p_{F(x,y)} = 1$ for foreground and $p_{F(x,y)} = 0$ for background
    \end{itemize}
    \item $w_f$: \begin{itemize}
        \item[$\cdot$] empirically observed background pixels versus foreground pixels ratio on the training set.
        \item[$\cdot$] to approximate it, we randomly sample M reference pictures from the training set and for each compute
        the number of foreground pixels $F_i =|\{(x,y): I(x,y)=1\}|$) and the number of background pixels $B_i =|\{(x,y): I(x,y)=0\}|$.
        \item[$\cdot$] $w_f = \frac{\sum_{i=0}^{M} B_i}{\sum_{i=0}^{M} F_i}$
    \end{itemize}
    \item $w_{(x,y)}$: \begin{itemize}
        \item [$\cdot$] weight for the pixel on the position (x,y)
        \item [$\cdot$]  \[
            w_{(x,y)} = \left\{\begin{array}{lr}
                w_f, & \text{if } p_{F(x,y)} = 1\\
                1, & \text{if } p_{F(x,y)} = 0\\
                \end{array} \right\} %= xy
          \]
        % \item 
        % \[
        %     w_{(x,y)} = \left\{\begin{array}{lr}
        %         w_f, & \text{if I_{(x,y)}=1 } \\
        %         1, & \text{for } 0\leq n\leq 1\\
        %         x(n-1), & \text{for } 0\leq n\leq 1
        %         \end{array}\right\} = xy
        %   \]    $w_{(x,y)} = $ 
    \end{itemize}
    \item $l_{(x,y)}$: \begin{itemize}
        \item[$\cdot$] loss for the pixel with coordinates $(x,y)$
        \item[$\cdot$] $l_{(x,y)} = - w_{(x,y)} [\cdot p_{F(x,y)} \cdot \log{\hat{p}_{F(x,y)}} + (1-p_{F(x,y)}) \cdot \log{(1-\hat{p}_{F(x,y)})}]$
    \end{itemize}
     
    \item $L(I)$: \begin{itemize}
        \item[$\cdot$] loss of the whole image I
        \item[$\cdot$] $L(I) =  \frac{1}{|I|} \cdot \sum_{(x,y) \in I} l_{(x,y)}$
    \end{itemize}
    
\end{itemize}

However, to increase the numerical stability, we implement the loss layer already combined with the previous sigmoid
layer. This does not change the loss defined above. It only increases the numerical stability due to the log-sum-exp trick
(for more details, see \cite{logsumexp}).
The combined layer is already available in Pytorch.

\subsection{Distance Transform Segmentation}

The main disadvantage of the previous approach is the following:\\
Suppose that classifier marks the pixel $A$ as the foreground.
However, the reference says the pixel $A$ is the background. The classifier is, therefore, wrong.
But the feedback is not really helpful. We do not get the information on how much wrong the prediction is,
neither the possible smooth improvement.

Contrary to the pixel-wise approach, the distance transform offers us more information about the mistake (how
far the prediction is).
Instead of the binary reference presented in the previous section, we use the distance from the closest
background pixel as the values we want to predict (inspired by Naylor \cite{naylor2018segmentation}).

The difference from the previous approach is the following:
\begin{enumerate}
    \item We need to transform the reference. Each pixel will now hold the distance to the closest background pixel in the original reference.
    \item We also need to change the loss function. Naturally, we choose the mean square error (MSE) loss as we try to predict the continuous variable (distance). The original paper also uses MSE. However,
     some of the nuclei are bigger than the others. We need to decide whether it would be beneficial to normalize the distance to the range $[0,1]$ (by the size of individual nuclei). Naylor \cite{naylor2018segmentation} suggests that the performance of the unnormalized setting is noticeably higher.
     Thus, we do not normalize.
\end{enumerate}

Formally, we define the loss as follows:
\begin{itemize}
    \item $I$: \begin{itemize}
        \item[$\cdot$] $I$ represents an input image, $I_{(x,y)}$ is a pixel value on the position $(x,y)$
        \item[$\cdot$] $|I|$ represents the number of pixels
    \end{itemize}

    \item $p_{F(x,y)}$: \begin{itemize}
        \item[$\cdot$] stands for reference value of the pixel with coordinates $(x,y)$
        \item[$\cdot$] can be interpreted as the ground truth probability that the cell is the foreground
        \item[$\cdot$] $p_{F(x,y)} = 1$ for foreground and $p_{F(x,y)} = 0$ for background
    \end{itemize}
    
    % \item $f(I)$ \begin{itemize}
    %     \item[$\cdot$] $f(I)$ is a function $R2 \rightarrow R$, which depends on the image 'I'
    %     \item[$\cdot$] $f(I)$ assigns each pixel $I_{(x,y)}$ value $v_{(x,y)} \in R_{0+}$
    %     \item[$\cdot$] $v_{(x,y)} = \min_{(x', y'), I_{(x',y')=0}} \{d((x,y), (x',y')) \}$, d is euclidean distance
    % \end{itemize}
    
    \item $v_{(x,y)}$ \begin{itemize}
        \item[$\cdot$] value $v_{(x,y)}$ expresses the euclidian distance from pixel with coordinates $(x,y)$ to the closest background 
                       pixel in the reference (ground truth)   
        \item[$\cdot$] $v_{(x,y)} = \min_{(x', y'), p_{F(x',y')}=0} \{d((x,y), (x',y')) \}$, d is euclidean distance
    \end{itemize}
    % \item $B$: \begin{itemize}
    %     \item[$\cdot$] transformed reference picture
    %     \item[$\cdot$] $B_{(x,y)} = f(I_{(x,y)})$ for each $(x,y)$ from $I$
    % \end{itemize}
    \item $\hat{B}$ \begin{itemize}
        \item[$\cdot$] the estimated distance map
        \item[$\cdot$] the neural network tries to predict for each $(x,y)$ the value $v_{(x,y)}$.
    \end{itemize}
    \item $L$ \begin{itemize}
        \item[$\cdot$] mean square error loss 
        \item[$\cdot$] $L = \frac{1}{|I|} \cdot \sum_{(x,y) \in I} (\hat{B}_{(x,y)} - v_{(x,y)})^2$
    \end{itemize}
        
    
\end{itemize}









