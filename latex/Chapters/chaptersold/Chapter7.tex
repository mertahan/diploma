
\chapter{The Proposed Solution}
\label{proposed_sol}

In this chapter, we will describe our prototype of the algorithm solving the problem described in the section above
 (\ref{formal_def}).

Firstly, let us examine the input data.
\section{The Datastructure} \label{data}
The input data consists of files containing reads sequenced by Illumina \cite{illumina} . 
These reads originate from multiple subjects. 

Each subject has exactly two corresponding files. The first file contains forward reads 
(see Section \ref{terminology}), the other contains reverse reads.

However, we have no further information to determine, which one of the two files contains forward reads. \footnote{ Reminder:
Forward reads originate from one strand of DNA and reverse reads from the complementary strand.
Moreover, reverse reads are sequenced from the opposite end of the strand.}


\begin{description}
    \item To provide a simple example:
    \begin{description}
        \item Suppose Strand1 is ACCTG.
        \item Complementary strand then results to TGGAC. 
        \item Forward read of length 5 is ACCTG (corresponding to Strand1)
        \item Reverse read of length 5 is CAGGT (this sequence is reversed complement to ACCTG as defined in Section \ref{formal_def}) 
    \end{description}

\end{description}

\noindent It will be necessary to keep this in mind when processing files.

\begin{description}
    \item Notation:
    We will refer to forward and corresponding reverse file as to R1-file and R2-file. 
    Remember, that we cannot distinguish which of them contains forward reads.
\end{description}

Now, we will outline the algorithm. It consists of 4 steps, and we will discuss each of them in the following 
sections.

\section{Step One : Computing k-mers Occurences in All Files}
\label{step_1}

As we show in Section \ref{main_idea}, our aim is to assign each read a corresponding gene expression. However,
read-sequences are too long (around 75-100 nucleotides, depending on a sequencing technology). Therefore, we will take 
shorter sequences called k-mers (see definition in Section \ref{formal_def}). 
A length of k-mer is roughly 25. We will discuss a better estimation of k in Section \ref{param_set}.
Consequently, we will compute a number of occurrences of each k-mer present for each file.

As we do not know, if a k-mer originated from a forward or a reverse read, we will merge the k-mer and its 
reverse complement (defined in \ref{formal_def}), keeping only one of them and assigning it a sum of both occurrences.
It is common to keep the k-mer that comes first lexicographically.

To compute counts for each file in the dataset we will use jellyfish \cite{jellyfish2010}. 
The main advantage of this tool is efficient and parallel counting all k-mers.

In the next section, we will process these k-mers and create a table.

\section{Step Two : Creating a Table of k-mers}
\label{step_2}

In this step, we will construct a table, where rows will represent all k-mers found in all files of datasets.
The occurrences of a particular k-mer in each column will refer to one subject from the dataset, containing occurrences of a
particular k-mer in reads associated with that subject.

That means summing a k-mer occurrences in R1-file and R2-file for every subject.
Moreover, we pad zeroes to k-mers that do not occur in every subject.
However, the k-mers that occur only few times in each file are not significant, as they provide a few information. Hence, we filter out 
k-mers that do not exceed lower bound %(10 by default) 
in any of the input files. 

A result of this step is similar to Figure \ref{image_expr}, only
 A and B are k-mers, and the dimension corresponds to the number of subjects.
Finally, k-mer counts represent expression of a particular k-mer (similarly to a gene expression).

\section{Step Three : Clustering k-mers}
\label{step_3}

In the previous step, we generated a table of k-mer counts. 
We can imagine this table as a set of rows, where each row consists of a k-mer and N-dimensional vector (N is be
the number of subjects).

In this step, it is essential to realize, that gene expression (amount of a gene transcribing) almost equally increases all
k-mer counts present in that gene. Except for k-mers at the ends of the reads as illustrated in Section \ref{cluster_method}.

Thus, similar counts at all positions of two k-mer vectors in the previously constructed table would 
indicate membership to the common gene.

Note, that this approach does not distinguish between uniformly distributed genes.
That is the reason, why it is important to have multiple different subjects.

So, relating to the idea that k-mers belonging to the same gene are close to each other in the vector space we created,
we proceed to the clustering of k-mers.

\subsection{Clustering Method Selection}
\label{cluster_method}

We considered multiple vector-based clustering algorithms (see Chapter \ref{Chapter4}
). For instance, agglomerative clustering, k-means clustering, and DBSCAN clustering discussed in Chapter \ref{Chapter4}.
All of them are implemented in sklearn python library \cite{sklearn}. 

However, due to the huge amount of data it is impossible to cluster all of the k-mers present in the table.
Luckily, it is not necessary. As one read sequence has length \footnote{$l$ is usually roughly $100$, in our case $l$ equals $76$}  $l$ , it contains $l - k + 1$ k-mers (where k usually varies from 20 to 30). However, these k-mers overlap. In fact,
most of the read sequence positions are covered by $k$ k-mers (except for the ends of read sequence). Therefore, we do not need to cluster all of the k-mers,
as the read can be determined by a smaller portion of them. 

Nonetheless, k-means and agglomerative clustering run out of memory very fast, even when taking only a small sample 
from the table.
On the other hand, DBSCAN, unlike the previous two algorithms, has an eps-bound, causing a rapid decrease of comparisons.
The eps-bound determines the maximum distance within a cluster. That means dividing the vector space into subsections that
do not need to be compared.

Due to these reasons, we decided to use sklearn \cite{sklearn} DBSCAN clustering, allowing us to take 10\% of the table constructed in step two and process it in a reasonable time. Determining eps will be discussed later in Section \ref{param_set}.  %%ref parameter set

Then, results of DBSCAN are the labels for each vector and $-1$ label for noise.
Finally, we sort clusters by size.

\subsection{Selecting the Imporant Clusters}
\label{important_cat}

When provided with clusters from DBSCAN clustering, we sort them by size. The largest cluster is noise 
and we do not further process it.

The second cluster should contain k-mers that are uniformly distributed and does not offer much information (roughly \footnote{while the number of human genes is
more than 40000 \cite{genesnumber}} 10000)
k-mers belong to that cluster).
On the other hand, we are interested in middle-sized clusters.
These follow right after the second cluster and contain roughly 10-50 k-mers.

We will keep in memory which clusters belong to this \textbf{\textit{important cathegory}}.

Finally, we define it as follows: \\
\textit{important cathegory} \textbf{I} = $\{ \forall  n : n \in \mathbb{N} \land n \in [v,u] \}, v < u $, where $u-v$ is a number of 
    important clusters. Here u stands for the upper bound and v for the lower bound, in our case the upper bound is 50 and lower bound 2. 
    This means that we mark the second, the third $\cdots$ the 50-th largest cluster as important.

\subsection{Visualizing Results}

As the vectors have more dimensions than 3, it is hard, though not impossible to visualize result clusters.
Hence, we use PCA-dimension reduction implemented in sklearn \cite{sklearn} to transform vectors to 2-dimensions and plot some of the
clusters (each cluster having a distinct color in the transformed vector space).

However, the outliers squish the axes so that the most of the points merge. 
Therefore, we use the z-score filtering to remove these distant points (with z equals 3).


The number of plotted clusters can be set, but for illustration purposes, we choose 17 clusters from the \textbf{\textit{important cathegory}} 
discussed above (omitting noise and uniformly distributed k-mers).

We need to remember that we squished vector space to a smaller dimension. Hence some clusters project to 
the similar place.

In the Figure \ref{dbclust},
 we show the PCA-reduced graphs of first 17 k-mer clusters with multiple different setting of $k$ and $eps$ and dataset.
  The same colored points 
belong to one cluster.
We can observe that the points belonging to the same cluster are projected into the similar position.

% \begin{figure}[!hbt]
%     \centerline{
%     \includegraphics[width=90mm]{Figures/res20_6PCA_2D_clusters1.png}
%     }
%     \caption{Clusters of k-mers projected to 2D, k = 20, eps = 6, dataset = 1}
%     \label{dbclust}
% \end{figure} 

\begin{figure}%
    \centering
    %\subfloat[label 1]{{\includegraphics[width=7.2cm]{Figures/N-eps1.png} }}%
    \subfloat[k = 20, eps = 6, dataset 1]{{\includegraphics[width=7.4cm]{Figures/res20_6PCA_2D_clusters1.png} }}%
    %\caption{haha}
    \qquad
    \hskip -9ex
    \subfloat[k = 24, eps = 6, dataset 2]{{\includegraphics[width=7.4cm]{Figures/res24_6PCA_2D_clusters.png} }}%

    \subfloat[k = 25, eps = 10, dataset~2]{{\includegraphics[width=7.4cm]{Figures/res25_10PCA_2D_clusters.png} }}%
    %\caption{haha}
    \qquad
    \hskip -9ex
    \subfloat[k = 25, eps = 12, dataset~2]{{\includegraphics[width=7.4cm]{Figures/res25_12PCA_2D_clusters.png} }}%

    \caption{Clusters of k-mers projected to 2D}%
    \label{dbclust}%
\end{figure}

% \begin{figure}%
%     \centering
%     \subfloat[label 1]{{\includegraphics[width=7.2cm]{Figures/N-eps1.png} }}%
%     \subfloat[label 1]{{\includegraphics[width=7.4cm]{Figures/res25_10PCA_2D_clusters.png} }}%
%     \caption{haha}
%     \qquad
%     \hskip -9ex
%     \subfloat[label 2]{{\includegraphics[width=7.4cm]{Figures/res25_12PCA_2D_clusters.png} }}%
%     \caption{Errors }%
%     \label{k-error}%
% \end{figure}


%res20_6PCA_2D_clusters_noise1.png

However, as the points originally have more dimensions (10), some of the points of different clusters overlap in the 2D space.

To illustrate the difference between the clusters including and excluding noise, we show the DBSCAN plot with noise in Figure \ref{dbclustnoise}.
Obviously, the noise provides no information about the clusters. Furthermore, it completely shadows the other points (as the noise contains much more
points than the not-noise points that we plot).

\begin{figure}[!hbt]
    \centerline{
    \includegraphics[width=90mm]{Figures/res20_6PCA_2D_clusters_noise1.png}
    }
    \caption{Clusters of k-mers projected to 2D including the noise}
    \label{dbclustnoise}
\end{figure} 





\section{Step Four : Clustering Reads}
\label{step_4}

In the previous step, we created clusters of k-mers based on an expression.
However, our goal is to cluster the original reads in files.
Hence, we need to cluster reads. As a read can belong to multiple genes, we will use soft clustering.
That means a read can be partially assigned to multiple clusters, each with some percentage summing to 1.

The result will be a membership table as defined in Section \ref{formal_def}. Each read will have a vector of membership to the previously
constructed clusters.

Firstly, we will process all data files containing reads (as specified in Section \ref{data}).
We will process data from R1-file and R2-file simultaneously.
For processing reads, we will use the python library Bio \cite{biopython}.
For each read A, we determine to which clusters it belongs.

We will do that as follows: \\
\begin{enumerate}
    \item If $\textrm{k-mer} \in R_m(A)$, then add 1 to membership vector on position corresponding with the cluster containing k-mer.
    \item We act the same if $ \textrm{k-mer} \in R_m( \textrm{reverse complement of A})$. That is because k-mer and the reverse
    complement of k-mer are equivalent (as described in step one).
    \item After that, we will normalize the vector of membership by the sum of the elements to obtain percentual
    membership.
\end{enumerate}

As a result, we will have a table of membership, determining for each read to which clusters it belongs.

To analyze if these clusters correspond with real genes, we will use the standard reference-based approach for 
comparison.
We will describe the analysis by STAR \cite{dobin2013star} 
in the next chapter.